package tg.agir.ong.agir_app;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import tg.agir.ong.agir_app.chat.ChatActivity;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.fragment.DeclarationMenuFragment;
import tg.agir.ong.agir_app.fragment.FragmentDeclaration;
import tg.agir.ong.agir_app.fragment.FragmentMap;
import tg.agir.ong.agir_app.fragment.FragmentSensitization;
import tg.agir.ong.agir_app.recyclerview.ItemObject;
import tg.agir.ong.agir_app.upload.ChatBroadCastReceiver;
import tg.agir.ong.agir_app.upload.ChatService;
import tg.agir.ong.agir_app.upload.UploadFileAsync;
import tg.agir.ong.agir_app.util.ConfigurablePager;
import tg.agir.ong.agir_app.util.Util;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class WelcomeActivity extends FragmentActivity {
    private TabLayout tablayout;
    private AppBarLayout appBarLayout;
    private ConfigurablePager viewPager;
    private DeclarationMenuFragment te;
    private FragmentDeclaration fragmentDeclaration;

    private double latitude;
    private double longitude;
    private Connection dbConnection;
    private WebSocketClientManager webSocketClientManager;
    public static String activeFragment;

    private static final String AUDIO_FILE_PATH =
            Environment.getExternalStorageDirectory().getPath() + "/";
    private static String FILE_NAME = "";
    private static String AUDIO_FILE_NAME = "";
    private Intent chatIntent;
    private static final int REQUEST_MAP = 0;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    static final int REQUEST_FORM_CAPTURE = 2;
    private static final int REQUEST_RECORD_AUDIO = 3;
    private FragmentMap fragmentCenters;
    private FragmentSensitization fragmentSensitization;

    private Intent map;

    private LinearLayoutManager lLayout;
    private ProgressDialog dialog;
    private Intent startServiceIntent;
    public static final String BROADCAST = "upload.android.action.sync";
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          //  fragmentDeclaration.getmAdapter().notifyDataSetChanged();
            if(fragmentDeclaration!=null)
            fragmentDeclaration.updateList();
            Log.e("Intent ", "INtent recu j'actualise!");

        }
    };

    public WelcomeActivity() {
        activeFragment = "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dialog = new ProgressDialog(WelcomeActivity.this);
        //Resources res = getResources();
        Log.i("onCreateWel", "enter");
        setTitle(getResources().getString(R.string.app_name));
        chatIntent = new Intent(getBaseContext(), ChatActivity.class);
        map = new Intent(getBaseContext(), MapActivity.class);
        super.onCreate(savedInstanceState);

        ChatService chatServiceIntent = new ChatService(getBaseContext());
        ChatBroadCastReceiver chatBroadCastReceiver = new ChatBroadCastReceiver();

        IntentFilter intentFilter = new IntentFilter(BROADCAST);
        registerReceiver(chatBroadCastReceiver, intentFilter);
        Intent i = new Intent(BROADCAST);
        sendBroadcast(i);


        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("fetch_data"));

        setContentView(R.layout.activity_welcome);


        dbConnection = new Connection(this);


        // Open the connection with the server
        webSocketClientManager = new WebSocketClientManager(WelcomeActivity.this, dbConnection);
        webSocketClientManager.connectWebSocket();

        tablayout = (TabLayout) findViewById(R.id.tablayout_id);
        appBarLayout = (AppBarLayout) findViewById(R.id.applicationbar_id);
        viewPager = (ConfigurablePager) findViewById(R.id.viewpager_id);
        viewPager.enableTouches();
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int position) {
                if (position == 1) {
                    // hide(tablayout);
                    hide(appBarLayout);
                    viewPager.disableTouches();
                } else if (tablayout.getVisibility() == View.GONE || appBarLayout.getVisibility() == View.GONE) {
                    show(appBarLayout);
                    show(tablayout);
                    viewPager.enableTouches();

                }

            }

        });
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        te = new DeclarationMenuFragment();
        fragmentCenters = new FragmentMap();
        fragmentDeclaration = new FragmentDeclaration();
        fragmentSensitization = new FragmentSensitization();
        adapter.addFragment(fragmentDeclaration, "DECLARATION");
        adapter.addFragment(fragmentCenters, "CENTRES");
        adapter.addFragment(fragmentSensitization, "NOUVELLES");

        viewPager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewPager);

        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    // goToMapActivity();
                    // viewPager.setCurrentItem(1);

                    boolean done = getSupportFragmentManager().popBackStackImmediate();
                    //  tablayout.setVisibility(View.GONE );
                    appBarLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    public static String getActiveFragment() {

        return activeFragment;
    }

    public void goToMapActivity() {
        Intent mapIntent = new Intent(this, MapActivity.class);
        startActivity(mapIntent);

    }

    public static void setActiveFragment(String activeFragment) {
        WelcomeActivity.activeFragment = activeFragment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        if (requestCode == REQUEST_RECORD_AUDIO) {
            if (resultCode == RESULT_OK) {

                // TODO: Send the file directly on the server and remove it locally
                ItemObject item = new ItemObject("Declaration Audio du " + dateFormat.format(currentTime), "Votre declaration a été envoyée ", R.drawable.volume, currentTime);
                item.setAudio_path(AUDIO_FILE_NAME);
                item.setText(item.getName());
                Util.getAllItems().add(item);
                Util.saveSharedPreferencesObjectList(this.getBaseContext(), Util.getAllItems());


                UploadFileAsync test = new UploadFileAsync(this, this.getApplicationContext(), item);
                test.execute(FILE_NAME);


            } else if (resultCode == RESULT_CANCELED) {
               // Toast.makeText(this, "Audio was not recorded", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Uri videoUri = data.getData();
            ItemObject item = new ItemObject("Declaration Video du " + dateFormat.format(currentTime), "Votre declaration a été envoyée ", R.drawable.video_camera, currentTime);
            item.setVideo_path(Util.getNameFromUri(getBaseContext(), videoUri));
            item.setText(item.getName());
            Util.getAllItems().add(item);
            Util.saveSharedPreferencesObjectList(this.getBaseContext(), Util.getAllItems());
            //   this.uploadFile(Util.getRealPathFromURI(getBaseContext(),videoUri));
            UploadFileAsync test = new UploadFileAsync(this, this.getApplicationContext(), item);

            test.execute(Util.getRealPathFromURI(this.getApplicationContext(), videoUri));

        }
        if (requestCode == REQUEST_FORM_CAPTURE && resultCode == RESULT_OK) {
            Util.getAllItems().add(Util.getSample_ItemObject());
            Util.saveSharedPreferencesObjectList(this.getBaseContext(), Util.getAllItems());
            UploadFileAsync test = new UploadFileAsync(this, this.getApplicationContext(), Util.getSample_ItemObject());
            test.execute("");
        }
    }

    public void recordAudio(View v) {
        if (Util.hasPermission(this, Manifest.permission.RECORD_AUDIO) &&
                Util.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            launchAudio();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 3);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);

        }
    }

    public void startChat(View v) {
        startActivity(chatIntent);
    }

    public void startVideo(View v) {
        if (Util.hasPermission(this, Manifest.permission.CAMERA) &&
                Util.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            launchVideo();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }
    }

    public void launchForm(View v) {
        //TODO ajouter le chargement de WriteLaterDeclarationActivity
        Intent intent = new Intent(this, StepActivity.class);
        startActivityForResult(intent, REQUEST_FORM_CAPTURE);
    }
    public void launchGuide(View v) {
        String url = getResources().getString(R.string.url_website);

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

    }

    public void launchAudio() {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH_mm_ss");
        String text = dateFormat.format(currentTime).replace('/', '_');
        AUDIO_FILE_NAME = "" + text + ".wav";
        FILE_NAME = AUDIO_FILE_PATH + "" + text + ".wav";
        AndroidAudioRecorder.with(this)
                // Required
                .setFilePath(FILE_NAME)
                .setColor(ContextCompat.getColor(this, R.color.colorBleueclaim))
                .setRequestCode(REQUEST_RECORD_AUDIO)

                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(false)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
    }

    public void launchVideo() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0) {
                    this.fragmentCenters.onPermissionResult(true);
                }
                return;
            }
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchVideo();
                }
                return;
            }
            case 3: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!Util.hasPermission(this, Manifest.permission.RECORD_AUDIO)) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
                        return;
                    }
                    if (!Util.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_RECORD_AUDIO);
                        return;
                    }
                    launchAudio();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    public FragmentDeclaration getFragmentDeclaration() {
        return fragmentDeclaration;
    }

    public void setFragmentDeclaration(FragmentDeclaration fragmentDeclaration) {
        this.fragmentDeclaration = fragmentDeclaration;
    }

    // slide the view from below itself to the current position
    public void hide(View view) {
        final View v = view;
        view.animate()
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(View.GONE);
                    }
                });

    }

    // slide the view from its current position to below itself
    public void show(View view) {
// Prepare the View for the animation
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.0f);

// Start the animation
        view.animate()
                .alpha(1.0f)
                .setListener(null);
    }

    @Override
    protected void onDestroy() {
        if(startServiceIntent!=null)
        stopService(startServiceIntent);

        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }
}