package tg.agir.ong.agir_app.adapter;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.entities.Chat;

public class ListChatRecyclerViewAdapter extends RecyclerView.Adapter<ListChatRecyclerViewAdapter.MyViewHolder> {



    private   List<Chat> listExpert;

    public ListChatRecyclerViewAdapter(List<Chat> listExpert) {
        this.listExpert = listExpert;
    }

    @Override
    public int getItemCount() {
        return this.listExpert.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // CREATE VIEW HOLDER AND INFLATING ITS XML LAYOUT
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.model_view_discut, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.display(this.listExpert.get(position));
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private  TextView name = ((TextView) itemView.findViewById(R.id.text_nom));
        private  TextView Title= ((TextView) itemView.findViewById(R.id.text_titre));
        private  ImageView Image_profile = ((ImageView) itemView.findViewById(R.id.Image_profile));

        public MyViewHolder(final View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.text_nom);
            Title =  itemView.findViewById(R.id.text_titre);
            Image_profile = itemView.findViewById(R.id.Image_profile);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  new AlertDialog.Builder(itemView.getContext())
                    //      .setTitle(currentPair.first)
                    //     .setMessage(currentPair.second)
                    //   .show();
                }
            });
        }

        public void display(Chat listClaims) {


            Title.setText(listClaims.getContent_text());
            name.setText(listClaims.getContent_text());


        }
    }}

