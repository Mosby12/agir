package tg.agir.ong.agir_app.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import tg.agir.ong.agir_app.AllInstantDeclarationMenuActivity;
import tg.agir.ong.agir_app.AllLaterDeclarationMenuActivity;
import tg.agir.ong.agir_app.AudioInstantDeclarationActivity;
import tg.agir.ong.agir_app.HexagonMaskView;
import tg.agir.ong.agir_app.ListDeclarationActivity;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WriteLaterDeclarationActivity;

public class DeclarationMenuFragment extends Fragment {
    private HexagonMaskView fragment_declaration_menu_imageview1;
    private HexagonMaskView fragment_declaration_menu_imageview2;
    private HexagonMaskView fragment_declaration_menu_imageview3;
    private HexagonMaskView fragment_declaration_menu_imageview4;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_declaration_menu, container, false);
        init();
        actions_on_click();
        return view;
    }

    public void init(){
        fragment_declaration_menu_imageview1 = view.findViewById(R.id.fragment_declaration_menu_imageview1);
        fragment_declaration_menu_imageview2 = new HexagonMaskView(view.getContext());
        fragment_declaration_menu_imageview3 = view.findViewById(R.id.fragment_declaration_menu_imageview3);
        fragment_declaration_menu_imageview4 = view.findViewById(R.id.fragment_declaration_menu_imageview4);
    }

    public void actions_on_click(){
        fragment_declaration_menu_imageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(), "Instantannée", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), AllInstantDeclarationMenuActivity.class);
                startActivity(intent);
            }
        });

        fragment_declaration_menu_imageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(), "Liste des déclarations", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), ListDeclarationActivity.class);
                startActivity(intent);
            }
        });

        fragment_declaration_menu_imageview4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(), "Ultérieure", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), AllLaterDeclarationMenuActivity.class);
                startActivity(intent);
            }
        });
    }

}
