package tg.agir.ong.agir_app.database_connexion.dao;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import tg.agir.ong.agir_app.entities.Experts;

public class ExpertsDAO extends DAO<Experts>{

    private static final String table_name = "experts";

    private static final  String column_id_expert="id_expert";
    private  static final  String column_expert_lastname="expert_lastname";
    private  static final String column_expert_firstname="expert_firstname";
    private  static final String column_expert_photo="expert_photo";
    private static final  String column_expert_email="expert_email";
    private  static final String column_expert_type="expert_type";
    private  static final String column_expert_contact1="expert_contact_1";
    private  static final String column_expert_contact2="expert_contact_2";
    private  static final String column_delete_state="delete_state";

    private  static   String Query;

    public ExpertsDAO(SQLiteDatabase pcon) {
        super(pcon);
    }

    public Experts search_Expert(SQLiteDatabase database){

        Experts exp = new Experts();

        Cursor cursor = database.rawQuery("SELECT * FROM expert ", new String[] {});

        if(cursor != null){

            return  new Experts();
        }

        return exp;
    }

    public void addexpert(){
        String query =" INSERT INTO "+table_name+"("+column_id_expert+","+column_expert_lastname+","+column_expert_firstname+","+column_expert_photo+","+column_expert_email+","+column_expert_type+","+column_expert_contact1+","+column_expert_contact2+","+column_delete_state+") VALUES(58,20,2,2,2,2,2,2,5)";

       // ContentValues values = new ContentValues();
       // values.put(column_id_expert,0);
        //values.put(column_expert_lastname,1);
        //values.put(column_expert_firstname,2);
        //values.put(column_expert_firstname,3);
        //db.insert(table_name,null,values);
      db.execSQL(query);
    }


    public static List<Experts> get_All_Expert(SQLiteDatabase database){
         Query=  "SELECT * FROM experts";
       List<Experts> lstExpert = new ArrayList<>();
        Cursor cursor = database.rawQuery(Query, null);
        if(cursor.moveToFirst()){
            do {

                Experts exp = new Experts();
                exp.setId_expert(cursor.getInt(0));
                lstExpert.add(exp);
            }
                while (cursor.moveToNext());
        }
        return lstExpert;
    }
}
