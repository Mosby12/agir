package tg.agir.ong.agir_app.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.articles.ArticlesDataModel;
import tg.agir.ong.agir_app.recyclerview.ItemObject;
import tg.agir.ong.agir_app.recyclerview.RecyclerViewAdapter;
import tg.agir.ong.agir_app.util.Util;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.RectanglePromptBackground;
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal;

public class FragmentDeclaration extends Fragment {
    View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DividerItemDecoration dividerItemDecoration;

    public FragmentDeclaration() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.declaration_fragment, container, false);
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        Boolean firstAccess = preference.getBoolean("firstAccess", true);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        if (firstAccess) {
            final SharedPreferences.Editor editor = preference.edit();

            new MaterialTapTargetPrompt.Builder(this.getActivity())
                    .setTarget(view.findViewById(R.id.fab))
                    .setPrimaryText("Bienvenue dans le service de declaration participatif")
                    .setSecondaryText("Appuyez pour selectionner le type de declaration que vous souhaitez faire ")
                    .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener() {
                        @Override
                        public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state) {
                            if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                                new MaterialTapTargetPrompt.Builder(getActivity())
                                        .setTarget(R.id.tablayout_id)
                                        .setPrimaryText("Le Menu  lateral ")
                                        .setSecondaryText("Elle vous permet de naviguer entre  la carte des centres de santé , et les actualités de l'ONG  ")
                                        .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener() {
                                            @Override
                                            public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state) {
                                                if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                                                    // User has pressed the prompt target

                                                }
                                                editor.putBoolean("firstAccess", false);
                                                editor.commit();
                                            }
                                        }).setPromptBackground(new RectanglePromptBackground())
                                        .setPromptFocal(new RectanglePromptFocal())
                                        .show();
                            }
                        }
                    })
                    .show();

        }
        mRecyclerView = view.findViewById(R.id.recycler_claims_view);

        // Reading list of screening centers in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        String screeningCentersList = preferences.getString("globalClaimsList", "");

        if (!screeningCentersList.equals("")) {

            String formattedArticlesList = screeningCentersList.replace("_FIRST_POINT_", ":");
            Log.i("customIarticl5424", "" + formattedArticlesList);
            final Map<String, String> articlesDataMap = new Gson().fromJson(formattedArticlesList, Map.class);
            Log.i("customIarticllast", "enter");

            List<ArticlesDataModel> dataModelList = new ArrayList<>();
            TextView nothingToDisplayTextView = view.findViewById(R.id.text_view_label_nothing_to_display);
            nothingToDisplayTextView.setVisibility(View.INVISIBLE);

            for (int i = 0; i < articlesDataMap.size(); i++) {

                String subFormattedArticlesList = articlesDataMap.get("" + i).
                        replace("_SECOND_POINT_", "\":\"");
                subFormattedArticlesList = subFormattedArticlesList.replace("_OPEN_ACCOLADE_", "{\"");
                subFormattedArticlesList = subFormattedArticlesList.replace("_COMMA_", "\",\"");
                subFormattedArticlesList = subFormattedArticlesList.replace("_CLOSE_ACCOLADE_", "\"}");
                Log.i("customIarticlsecon", "" + subFormattedArticlesList);
                final Map<String, String> screeningCentersSubDataMap = new Gson().fromJson(subFormattedArticlesList, Map.class);

                dataModelList.add(new ArticlesDataModel(screeningCentersSubDataMap));

            }


            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager

            mLayoutManager = new LinearLayoutManager(view.getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            // specify an adapter and pass in our data model list

     /*   mAdapter = new ArticlesAdapter(dataModelList, view.getContext());
          mRecyclerView.setAdapter(mAdapter);
        */

        }

        //TODO Test de la presentation des listes... Cette ligne est à supprimer
        fetch_data();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//handling swipe refresh
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                        updateList();
                        mRecyclerView.smoothScrollToPosition(0);
                    }
                }, 2000);
            }
        });
        return view;
    }

    public void onPause() {
        super.onPause();
        Log.i("onPauseNews", (" is on screen"));
    }

    @Override
    public void onResume() {
        super.onResume();
        WelcomeActivity.setActiveFragment(this.getClass().getSimpleName());
        Log.i("onResumeNews", (" is on screen"));
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("onStopNews", (" is on screen"));
    }

    public void updateList() {
        if (dividerItemDecoration != null)
            mRecyclerView.removeItemDecoration(dividerItemDecoration);

        fetch_data();

        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();


    }

    public void fetch_data() {
        Type type = new TypeToken<List<ItemObject>>() {
        }.getType();
        List<ItemObject> rowListItem;
        if (Util.loadSharedPreferencesObjectList(view.getContext()).isEmpty()) {
            rowListItem = Util.getAllItemList();
        } else {
            rowListItem = Util.loadSharedPreferencesObjectList(view.getContext());
        }
        if (rowListItem.size() != 0) {
            TextView nothingToDisplayTextView = view.findViewById(R.id.text_view_label_nothing_to_display_2);
            nothingToDisplayTextView.setVisibility(View.INVISIBLE);
        }
        Util.saveSharedPreferencesObjectList(view.getContext(), rowListItem);
        Collections.sort(rowListItem, Collections.reverseOrder());
        mAdapter = new RecyclerViewAdapter(view.getContext(), rowListItem);

        dividerItemDecoration = new DividerItemDecoration(view.getContext(),
                LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }


    public RecyclerView.Adapter getmAdapter() {
        return mAdapter;
    }

    public void setmAdapter(RecyclerView.Adapter mAdapter) {
        this.mAdapter = mAdapter;
    }
}
