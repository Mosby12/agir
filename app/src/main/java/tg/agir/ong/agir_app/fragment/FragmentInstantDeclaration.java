package tg.agir.ong.agir_app.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.ViewPagerAdapter;

public class FragmentInstantDeclaration extends Fragment {
    View view;

    TextView u;

    private TabLayout tablayout1;
    private ViewPager viewPager1;
    private DeclarationMenuFragment declarationMenuFragment;
    public FragmentInstantDeclaration() {

    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.declaration_instant_fragment,container,false);
        declarationMenuFragment = new DeclarationMenuFragment();

        viewPager1 =  view.findViewById(R.id.declaration_fragment_menu_viewpager_id);
        ViewPagerAdapter adapter1 = new ViewPagerAdapter(getChildFragmentManager());
        adapter1.addFragment(declarationMenuFragment,"");

        viewPager1.setAdapter(adapter1);
        init();

        return view;
    }

    public void init(){

    }

}
