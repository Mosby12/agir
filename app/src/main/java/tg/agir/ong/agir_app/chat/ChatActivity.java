package tg.agir.ong.agir_app.chat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import omrecorder.AudioChunk;
import omrecorder.AudioSource;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.Recorder;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.upload.ChatAsync;
import tg.agir.ong.agir_app.util.Util;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Varun John on 4 Dec, 2018
 * Github : https://github.com/varunjohn
 */
public class ChatActivity extends AppCompatActivity implements AudioRecordView.RecordingListener {

    private AudioRecordView audioRecordView;
    private View back;

    private RecyclerView recyclerViewMessages;
    private MessageAdapter messageAdapter;
    private List<Message> messageList;
    private long time;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;
    Recorder recorder;
    TextView header_text;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //  fragmentDeclaration.getmAdapter().notifyDataSetChanged();
            if (messageAdapter != null) {
                messageAdapter.notifyDataSetChanged();
                initView();
                Log.e(" Chat  ", "INtent recu j'actualise les chats!");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        initView();
        if (!(Util.hasPermission(this, Manifest.permission.RECORD_AUDIO) &&
                Util.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
            requestPermission();
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("refresh_chat"));

        setListener();
    }

    private void setListener() {

        audioRecordView.getAttachmentView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Attachment");
            }
        });

        audioRecordView.getSendView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = audioRecordView.getMessageView().getText().toString();
                if (!msg.isEmpty()) {

                    audioRecordView.getMessageView().setText("");
                    Message message = new Message(msg);
                    message.setId_claim(messageList.get(0).getId_claim());

                    ChatAsync test = new ChatAsync(ChatActivity.this, getBaseContext(), message);
                    test.execute("");
                    messageAdapter.add(message);
                }
            }
        });
    }

    @Override
    public void onRecordingStarted() {
        //showToast("Enregistrement en cours");
        debug("started");

        time = System.currentTimeMillis() / (1000);

        recorder = OmRecorder.wav(
                new PullTransport.Default(wavAudioSourcemic(), new PullTransport.OnAudioChunkPulledListener() {
                    @Override
                    public void onAudioChunkPulled(AudioChunk audioChunk) {
                    }
                }), file(time + "_demo.wav"));
        recorder.startRecording();


    }

    @Override
    public void onRecordingLocked() {
        // showToast("locked");
        debug("locked");
    }

    @Override
    public void onRecordingCompleted() {
        // showToast("completed");
        debug("completed");

        try {
            if (recorder != null)
                recorder.stopRecording();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int recordTime = (int) ((System.currentTimeMillis() / (1000)) - time);

        if (recordTime > 2) {
            Message message = new Message(recordTime);
            message.setId_claim(messageList.get(0).getId_claim());
            message.setAudio_path(time + "_demo.wav");
            messageAdapter.add(message);
            ChatAsync test = new ChatAsync(ChatActivity.this, getBaseContext(), message);
            test.execute(Environment.getExternalStorageDirectory().getPath() + "/" + message.getAudio_path());
        }

    }

    @Override
    public void onRecordingCanceled() {
        // showToast("Enregistement ANullé");
        debug("canceled");
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void debug(String log) {
        Log.d("VarunJohn", log);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    public MessageAdapter getMessageAdapter() {
        return messageAdapter;
    }

    public void setMessageAdapter(MessageAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    public void refreshData() {
        this.messageAdapter.notifyDataSetChanged();
    }

    public void initView() {
        audioRecordView = findViewById(R.id.recordingView);
        recyclerViewMessages = findViewById(R.id.recyclerViewMessages);
        back = findViewById(R.id.back_arrow);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        audioRecordView.setRecordingListener(this);

        messageAdapter = new MessageAdapter();

        messageList = ((List<Message>) getIntent().getExtras().getSerializable("list"));
        for (Message message : messageList) {
            messageAdapter.add(message);
        }

        updateHeader();

        recyclerViewMessages.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerViewMessages.setHasFixedSize(false);

        recyclerViewMessages.setAdapter(messageAdapter);
        recyclerViewMessages.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMessages.smoothScrollToPosition(messageList.size() - 1);
    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(ChatActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }


    public boolean checkPermission() {
        int result = ActivityCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ActivityCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(ChatActivity.this, "Merci",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ChatActivity.this, "Pour beneficier de toutes les fonctionnalités de l'application", Toast.LENGTH_LONG).show();
                        requestPermission();
                    }
                }
                break;
        }
    }

    @NonNull
    private File file(String value) {
        return new File(Environment.getExternalStorageDirectory(), value);
    }

    private AudioSource wavAudioSourcemic() {
        return new AudioSource.Smart(MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                AudioFormat.CHANNEL_IN_MONO, 44100);

    }

    public void updateHeader() {
        //TODO changer cette  ligne, ce n'est pas joli
        if (messageList.get(0).getText().contains("Video")) {
            Pattern p = Pattern.compile("du (.*)");
            Matcher m = p.matcher(messageList.get(0).getText());
            String s = messageList.get(0).getText();
            String title_without_date = messageList.get(0).getText();
            if (m.find()) {
                s = m.group(1); // " that is awesome"

            }

            title_without_date = messageList.get(0).getText().replaceAll("du (.*)", "");

            try {

                Date date = new SimpleDateFormat(Util.determineDateFormat(s), Locale.FRANCE).parse(s);
                String jour = new SimpleDateFormat("d MMMM yyyy").format(date);
                header_text = this.findViewById(R.id.chat_header);
                header_text.setText(title_without_date + " du " + jour);
                messageList.get(0).setText(title_without_date + " du " + jour);

            } catch (ParseException e) {
                //Handle exception here
                e.printStackTrace();
            }
        }

    }


    /*private PullableSource mic() {
        return new PullableSource.Default(
                new AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 44100
                )
        );
    }*/

}
