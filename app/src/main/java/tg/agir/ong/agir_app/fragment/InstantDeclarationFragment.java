package tg.agir.ong.agir_app.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tg.agir.ong.agir_app.AudioInstantDeclarationActivity;
import tg.agir.ong.agir_app.HexagonMaskView;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.VideoInstantDeclarationActivity;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.WriteInstantDeclarationActivity;


public class InstantDeclarationFragment extends Fragment {
    private HexagonMaskView fragment_instant_write_declaration_imageview;
    private HexagonMaskView fragment_instant_video_declaration_imageview;
    private HexagonMaskView fragment_instant_audio_declaration_imageview;
    private HexagonMaskView fragment_instant_return_declaration_imageview;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_instant_declaration, container, false);
        init();
        actions_on_view();
        return view;
    }

    public void init(){
        fragment_instant_write_declaration_imageview = view.findViewById(R.id.instant_write_declaration_imageview_bt);
        fragment_instant_video_declaration_imageview = view.findViewById(R.id.instant_video_declaration_imageview_bt);
        fragment_instant_audio_declaration_imageview = view.findViewById(R.id.instant_audio_declaration_imageview_bt);
        fragment_instant_return_declaration_imageview = view.findViewById(R.id.instant_return_declaration_imageview_bt);

    }

    public void actions_on_view(){
        fragment_instant_write_declaration_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), WriteInstantDeclarationActivity.class);
                startActivity(intent);
            }
        });

        fragment_instant_video_declaration_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), VideoInstantDeclarationActivity.class);
                startActivity(intent);
            }
        });



        fragment_instant_return_declaration_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), WelcomeActivity.class);
                startActivity(intent);
            }
        });
    }

}
