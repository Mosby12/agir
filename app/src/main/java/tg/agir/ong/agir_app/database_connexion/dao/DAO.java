package tg.agir.ong.agir_app.database_connexion.dao;

import android.database.sqlite.SQLiteDatabase;

public abstract class DAO<T> {
    public static SQLiteDatabase db;

    public DAO(SQLiteDatabase database) {
        this.db = database;
    }

}
