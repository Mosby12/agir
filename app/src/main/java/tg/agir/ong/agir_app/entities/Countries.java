package tg.agir.ong.agir_app.entities;

public class Countries {
    private int id_country;
    private String country_name;
    private String country_phone_code;
    private int delete_state;

    public Countries() {

    }

    public int getId_country() {
        return id_country;
    }

    public void setId_country(int id_country) {
        this.id_country = id_country;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_phone_code() {
        return country_phone_code;
    }

    public void setCountry_phone_code(String country_phone_code) {
        this.country_phone_code = country_phone_code;
    }

    public int getDelete_state() {
        return delete_state;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }
}
