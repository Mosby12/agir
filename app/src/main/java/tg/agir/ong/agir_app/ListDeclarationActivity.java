package tg.agir.ong.agir_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import tg.agir.ong.agir_app.database_connexion.dao.ClaimsDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.entities.Claims;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListDeclarationActivity extends AppCompatActivity {
    private Connection con;
    ListView listView;
    ArrayList<Claims> claimslist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_declaration);
        init();
        action_onclick();
    }

    public void init(){
        con = new Connection(this);
        listView = findViewById(R.id.list_declaration_list_view);

        final DAO<Claims> claimdao = new ClaimsDAO(con.getDb());
        claimslist = ((ClaimsDAO) claimdao).getAllClaiims();

        ListDeclarationActivity.ClaimsListAdapter c1 = new ListDeclarationActivity.ClaimsListAdapter();
        listView.setAdapter(c1);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Claims c23 = (Claims) listView.getAdapter().getItem(position);

                Toast.makeText(ListDeclarationActivity.this, "", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void action_onclick(){

    }

    public class ClaimsListAdapter extends BaseAdapter{
        private View view;
        private TextView v1;
        @Override
        public int getCount() {
            return claimslist.size();
        }

        @Override
        public Claims getItem(int position) {
            return claimslist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.list_declaration_item, null);
            v1 = findViewById(R.id.list_declaration_date_claim);


            return view;
        }
    }
}
