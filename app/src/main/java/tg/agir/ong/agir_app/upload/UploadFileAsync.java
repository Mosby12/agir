package tg.agir.ong.agir_app.upload;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.appcompat.app.AlertDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.chat.Message;
import tg.agir.ong.agir_app.recyclerview.ItemObject;
import tg.agir.ong.agir_app.util.Util;

public class UploadFileAsync extends AsyncTask<String, Void, String> {

    private WelcomeActivity currentActivity;
    private ProgressDialog p;
    private Context ctx;
    String url_params = "";
    String upload_folder = "portal_mofiala/assets/upload/claims_";
    String video_path = "", audio_path = "";
    FileInputStream fileInputStream;

    public UploadFileAsync(WelcomeActivity activity, Context ctx, ItemObject itemObject) {
        this.currentActivity = activity;
        this.ctx = ctx;
        this.p = new ProgressDialog(this.currentActivity);
        if (!itemObject.getAudio_path().isEmpty()) {
            audio_path = itemObject.getAudio_path();
        }
        if (!itemObject.getVideo_path().isEmpty()) {
            video_path = itemObject.getVideo_path();
        }
        this.url_params = "date=" + itemObject.getFormattedDate() + "&audio_path=" + audio_path
                + "&video_path=" + video_path + "&text_content=" + itemObject.getText();

    }

    /*
        Runs on the UI thread before doInBackground
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        p.setMessage("Envoi du fichier en cours");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
    }

    /*
        Runs on the UI thread after doInBackground
         */
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("OK")) {
            // Do something awesome here
            getClaimsFromDB(this.ctx);
        } else {
            Toast.makeText(this.ctx, "Echec de l'envoi verifier votre connexion", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String sourceFileUri = params[0];
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String fileName = sourceFileUri;
        HttpsURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.currentActivity);
        SharedPreferences.Editor editor = preferences.edit();
        String phoneNumber = preferences.getString("globalPhoneNumber", "");

        try {


            URL url = new URL("https://www.ongagir.com/upload.php?phone=+" + phoneNumber + "&" + this.url_params);
            trustAllHosts();
            // Open a HTTP  connection to  the URL
            conn = (HttpsURLConnection) url.openConnection();
            disableCertificateValidation(conn);
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("file", fileName);
            conn.setRequestProperty("phone", phoneNumber);


            if (!fileName.isEmpty()) {
                // open a URL connection to the Servlet
                //
                fileInputStream = new FileInputStream(sourceFile);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
            }
            // Responses from the server (code and message)
            int serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();

            Log.i("uploadFile", "HTTP Response is : "
                    + serverResponseMessage + ": " + serverResponseCode);

        } catch (MalformedURLException ex) {
            ex.printStackTrace();

            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
        } catch (Exception e) {


            Log.e("Failed  file to server", "error: " + e.getStackTrace(), e);


        }
        return "OK";

    } // End else block

    public void getClaimsFromDB(Context context) {
        final Context main_context = context;
        final List<ItemObject> itemObjects = new ArrayList<ItemObject>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        String phoneNumber = preferences.getString("globalPhoneNumber", "");
        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                String result = "KO";
                trustAllHosts();

                OkHttpClient client = getUnsafeOkHttpClient();
                Request request = new Request.Builder()
                        .url("https://www.ongagir.com/chat.php?phone=" + params[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONObject object_ = new JSONObject(response.body().string());
                    JSONArray array = object_.getJSONArray("claims");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);
                        String user_id = object.getString("user_id");

                        ItemObject itemObject = new ItemObject();
                        itemObject.setId_claim(object.getString("id_claim"));
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = simpleDateFormat.parse(object.getString("saved_on"));
                        itemObject.setDate(date);
                        itemObject.setName(object.getString("text_content"));
                        if (!object.getString("video_content").isEmpty()) {
                            itemObject.setVideo_path(object.getString("video_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.video_camera);
                        }
                        if (!object.getString("audio_content").isEmpty()) {
                            itemObject.setAudio_path(object.getString("audio_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.volume);

                        }
                        if (object.getString("audio_content").isEmpty() && object.getString("video_content").isEmpty()) {
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setText(object.getString("text_content"));
                            itemObject.setName("Declaration ecrite du " + simpleDateFormat.format(itemObject.getDate()));
                            itemObject.setPhotoId(R.drawable.medical_report);
                        }
                        JSONArray lineItems = object.getJSONArray("messages");

                        List<Message> messages = new ArrayList<>();
                        for (int k = 0; k < lineItems.length(); k++) {
                            JSONObject jsonLineItem = lineItems.getJSONObject(k);

                            String content_text = jsonLineItem.getString("content_text");
                            String audio_path = jsonLineItem.getString("context_audio");

                            Message message = new Message(content_text);
                            message.setId_claim(itemObject.getId_claim());
                            if (audio_path != null && !audio_path.isEmpty()) {
                                message.type = Message.TYPE_AUDIO;
                                message.setAudio_path(audio_path);
                            }
                            if (!user_id.equals(jsonLineItem.getString("id_sender"))) {
                                message.setIs_sender(false);
                            }
                            messages.add(message);

                        }
                        itemObject.setMessageList(messages);
                        itemObjects.add(itemObject);

                    }
                    result = "OK";

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                //adapter.notifyDataSetChanged();
                if (result.equals("OK")) {
                    p.dismiss();
                    Util.saveSharedPreferencesObjectList(main_context, itemObjects);
                    currentActivity.getFragmentDeclaration().updateList();
                   //Toast.makeText(ctx, "Envoi reussi ", Toast.LENGTH_SHORT).show();
                } else {
                    p.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(currentActivity);
                    builder.setTitle("Erreur reseau !!");
                    builder.setMessage("Réseau indisponible!");

                    builder.setPositiveButton("Reesayer ?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getClaimsFromDB(ctx);

                        }
                    });

                    AlertDialog dialog = builder.create();

                    dialog.show();
                }
            }
        };

        asyncTask.execute(phoneNumber);
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void trustAllHosts() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
        }
    }


    public void disableCertificateValidation(HttpsURLConnection conn) {
        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
        // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new TrustAllHostnameVerifier();

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            conn.setDefaultSSLSocketFactory(sc.getSocketFactory());
            conn.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {
        }
    }
}