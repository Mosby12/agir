package tg.agir.ong.agir_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import tg.agir.ong.agir_app.fragment.FragmentCenters;
import tg.agir.ong.agir_app.fragment.FragmentSensitization;
import tg.agir.ong.agir_app.fragment.InstantDeclarationFragment;
import tg.agir.ong.agir_app.fragment.LaterDeclarationFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

public class AllLaterDeclarationMenuActivity extends AppCompatActivity {

    private AppBarLayout appBarLayout;
    private TabLayout tablayout1;
    private ViewPager viewPager1;

    private LaterDeclarationFragment frament_later_declaration;
    private FragmentCenters fragment_centrals;
    private FragmentSensitization frangment_sensitization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_later_declaration_menu);

        init();
        action_on_click();
    }

    public void init(){
        frament_later_declaration = new LaterDeclarationFragment();
        fragment_centrals = new FragmentCenters();
        frangment_sensitization = new FragmentSensitization();
        appBarLayout =  findViewById(R.id.applicationbar_id);


        tablayout1 =  findViewById(R.id.declaration_menu_later_tablayout_id);
        viewPager1 =  findViewById(R.id.declaration_menu_later_viewpager_id);
        ViewPagerAdapter adapter1 = new ViewPagerAdapter(getSupportFragmentManager());

        adapter1.addFragment(frament_later_declaration,"DECLARATION");
        adapter1.addFragment(frangment_sensitization,"NOUVELLES");
        adapter1.addFragment(fragment_centrals,"CENTRES");

        viewPager1.setAdapter(adapter1);
        tablayout1.setupWithViewPager(viewPager1);
    }

    public void action_on_click(){

    }
}
