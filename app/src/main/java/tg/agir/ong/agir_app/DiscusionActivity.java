package tg.agir.ong.agir_app;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import tg.agir.ong.agir_app.adapter.ListExpertsRecyclerViewAdapter;
import tg.agir.ong.agir_app.database_connexion.dao.ChatDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.database_connexion.dao.ExpertsDAO;
import tg.agir.ong.agir_app.entities.Chat;
import tg.agir.ong.agir_app.entities.FolderAdministrator;

public class DiscusionActivity extends AppCompatActivity {

    ArrayList<FolderAdministrator> liste_Assig = new ArrayList<>();
//    Liste_view_Adapter listAdapt;

    protected Connection con;
    protected SQLiteDatabase db;

    RecyclerView list_discut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_discussion);
        final RecyclerView rv =  findViewById(R.id.listeDiscut);
        rv.setLayoutManager(new LinearLayoutManager(this));




        con = new Connection(this);

        DAO<Chat> chatDAO = new ChatDAO(con.getDb());
        ((ChatDAO) chatDAO).addmessage();
    //  DAO<Experts> expertDAO = new ExpertsDAO(con.getDb());
      //  ((ExpertsDAO) expertDAO).addexpert();
        rv.setAdapter(new ListExpertsRecyclerViewAdapter(ExpertsDAO.get_All_Expert(con.getDb())));


    }

}