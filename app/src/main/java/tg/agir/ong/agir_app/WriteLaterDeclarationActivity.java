package tg.agir.ong.agir_app;

import androidx.appcompat.app.AppCompatActivity;
import tg.agir.ong.agir_app.database_connexion.dao.CitiesDAO;
import tg.agir.ong.agir_app.database_connexion.dao.ClaimsDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.database_connexion.dao.DistrictDAO;
import tg.agir.ong.agir_app.entities.Cities;
import tg.agir.ong.agir_app.entities.Claims;
import tg.agir.ong.agir_app.entities.District;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class WriteLaterDeclarationActivity extends AppCompatActivity {
    private Connection con;
    private Claims claimtosave;
    private Cities city;
    private Button send_later_write_declaration;
    private EditText later_write_declaration_edit_text;
    private TextView select_town;
    private TextView select_district;
    private TextView select_date;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    private String date_abuse_occur;
    private int day;
    private int month;
    private int year;
    private ArrayList<District> alldistrict;

    String[] listcities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_later_declaration);
        init();
        actions();
    }

    public void init(){
        send_later_write_declaration = findViewById(R.id.send_later_write_declaration);
        later_write_declaration_edit_text = findViewById(R.id.later_write_declaration_edit_text);
        select_district = findViewById(R.id.write_later_discrict_select);
        select_town = findViewById(R.id.write_later_town_select);
        select_date = findViewById(R.id.write_later_select_date);
        claimtosave = new Claims();
        con = new Connection(this);

    }

    public void actions(){
         select_date.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 calendar = Calendar.getInstance();

                 datePickerDialog = new DatePickerDialog(WriteLaterDeclarationActivity.this, new DatePickerDialog.OnDateSetListener() {
                     @Override
                     public void onDateSet(DatePicker view, int Myear, int Mmonth, int MdayOfMonth) {
                         select_date.setText(MdayOfMonth + "/"+ (Mmonth+1) + "/" + Myear);
                     }
                 }, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
                 datePickerDialog.show();
             }
         });
         
         select_town.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 AlertDialog.Builder mBuilder = new AlertDialog.Builder(WriteLaterDeclarationActivity.this);
                 mBuilder.setTitle("Liste des villes");
                 CitiesDAO citiesDAO = new CitiesDAO(con.getDb());
                 ArrayList<Cities> arraycities = new ArrayList<>();
                 arraycities = citiesDAO.getAllCities();

                 final String table[];
                 table = new String[arraycities.size()];
                 for(int i = 0; i < arraycities.size(); i++){
                     Cities c = new Cities();
                     c = arraycities.get(i);
                     table[i] = c.getCity_name();
                 }
                 final ArrayList<Cities> finalArraycities = arraycities;
                 mBuilder.setSingleChoiceItems(table, -1, new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         Cities c = finalArraycities.get(which);
                         select_town.setText(table[which]);
                         Toast.makeText(WriteLaterDeclarationActivity.this, ""+c.getId_city(), Toast.LENGTH_SHORT).show();

                         alldistrict = new ArrayList<>();
                         DistrictDAO districtDAO = new DistrictDAO(con.getDb());
                         alldistrict = districtDAO.getAllDistrict(c.getId_city());

                     }
                 });

                 mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {

                     }
                 });

                 AlertDialog mDialog = mBuilder.create();
                 mDialog.show();
             }
         });

         select_district.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(alldistrict == null){
                     Toast.makeText(WriteLaterDeclarationActivity.this, "Veuillez selectionner une ville", Toast.LENGTH_SHORT).show();
                 }else {
                     Toast.makeText(WriteLaterDeclarationActivity.this, "Click sur la selection du quartier", Toast.LENGTH_SHORT).show();
                     AlertDialog.Builder mBuilder = new AlertDialog.Builder(WriteLaterDeclarationActivity.this);
                     mBuilder.setTitle("Liste des villes");
                     final String table[];
                     table = new String[alldistrict.size()];
                     for (int i = 0; i < alldistrict.size(); i++) {
                         District c = new District();
                         c = alldistrict.get(i);
                         table[i] = c.getDistrict_name();
                     }
                     final ArrayList<District> finalArraycities = alldistrict;
                     mBuilder.setSingleChoiceItems(table, -1, new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             District c = finalArraycities.get(which);
                             select_district.setText(table[which]);
                             claimtosave.setPlace_district(c.getId_district());
                             Toast.makeText(WriteLaterDeclarationActivity.this, "" + claimtosave.getPlace_district(), Toast.LENGTH_SHORT).show();


                         }
                     });

                     mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {

                         }
                     });

                     AlertDialog mDialog = mBuilder.create();
                     mDialog.show();
                 }
             }
         });

         send_later_write_declaration.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Calendar cal = Calendar.getInstance();
                 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                 String currentDateTimeString = dateFormat.format(cal.getTime());
                 claimtosave.setDate_claim_occur(select_date.getText().toString());
                 claimtosave.setText_content(later_write_declaration_edit_text.getText().toString());
                 claimtosave.setDeclaim(1);
                 claimtosave.setSaved_on(currentDateTimeString);
                 claimtosave.setClaim_status(2);
                 claimtosave.setDelete_state(1);
                 final DAO<Claims> claimdao = new ClaimsDAO(con.getDb());

                 if(select_date.getText().toString() == "" || select_town.getText().toString() == "" || select_district.getText().toString() == "" || later_write_declaration_edit_text.getText().toString() == ""){
                     Toast.makeText(WriteLaterDeclarationActivity.this, "Veuillez completer les champs. C'est pas encore dans string", Toast.LENGTH_SHORT).show();
                 }else{
                     try {
                         ((ClaimsDAO) claimdao).add_claims(claimtosave);
                         Toast.makeText(WriteLaterDeclarationActivity.this, "Cest bon", Toast.LENGTH_SHORT).show();
                     }
                     catch(Exception e){
                         Log.i("LaterWriteSend", e.getMessage());
                     }
                 }
             }
         });
    }
}
