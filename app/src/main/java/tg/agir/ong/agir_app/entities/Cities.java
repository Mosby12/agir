package tg.agir.ong.agir_app.entities;

public class Cities {
    private String id_city;
    private String city_name;
    private int from_region;
    private int delete_state;

    public Cities() {

    }

    public String getId_city() {
        return id_city;
    }

    public void setId_city(String id_city) {
        this.id_city = id_city;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public int getFrom_region() {
        return from_region;
    }

    public void setFrom_region(int from_region) {
        this.from_region = from_region;
    }

    public int getDelete_state() {
        return delete_state;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }
}
