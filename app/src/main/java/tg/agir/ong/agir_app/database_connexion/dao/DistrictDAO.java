package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import tg.agir.ong.agir_app.entities.District;

public class DistrictDAO extends DAO<District> {
    //table name
    private static final String table_name = "district";

    //Table columns
    private static final String column_id_region = "id_district";
    private static final String column_region_name = "district_name";
    private static final String column_from_city = "from_city";
    private static final String column_delete_state = "delete_state";

    public DistrictDAO(SQLiteDatabase database) {
        super(database);
    }

    public void add_district(District district){
        ContentValues values = new ContentValues();
        values.put(column_region_name, district.getDistrict_name());
        values.put(column_from_city, district.getFrom_city());
        values.put(column_delete_state, district.getDelete_state());
        long inserResult = db.insert(table_name,null,values);
        Log.i("table_nameinse", "table_nameinse " + inserResult);
    }

    public ArrayList<District> getAllDistrict(String i){

        Log.i("getAllUsers", "getAllUsers");
        String query =  "SELECT * FROM "+ table_name + " WHERE " + column_from_city + " = " + i + " AND " + column_delete_state + " = 0";
        ArrayList<District> districtList = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                District district = new District();
                district.setId_district(cursor.getInt(0));
                district.setDistrict_name(cursor.getString(1));
                district.setFrom_city(cursor.getString(2));
                district.setDelete_state(cursor.getInt(3));
                districtList.add(district);
            }
            while (cursor.moveToNext());
        }
        Log.i("getAllUsersEnd", "getAllUsersEnd");
        return districtList;
    }
}
