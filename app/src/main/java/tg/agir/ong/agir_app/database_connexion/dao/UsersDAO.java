package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import tg.agir.ong.agir_app.entities.Chat;
import tg.agir.ong.agir_app.entities.Users;

public class UsersDAO extends DAO<Users> {
    private static final String table_name = "users";

    //Table users columns
    private static final String column_id_user = "id_user";
    private static final String column_user_lastname = "user_lastname";
    private static final String column_user_firstname = "user_firstname";
    private static final String column_user_youngname = "user_youngname";
    private static final String column_user_pseudo = "user_pseudo";
    private static final String column_user_birthdate = "user_birthdate";
    private static final String columnn_user_nationality = "user_nationality";
    private static final String column_user_phone_number = "user_phone_number" ;
    private static final String column_sexual_tendencies = "sexual_tendencies";
    private static final String column_marital_status = "matital_status";
    private static final String column_is_employed = "is_employed";
    private static final String column_profession = "profession";
    private static final String column_user_email = "user_email";
    private static final String column_user_password = "user_password";
    private static final String column_serological_status = "serological_status";
    private String chat_password;
    private static final String column_user_account_confirmation = "user_account_confirmation";
    private String user_password_recover_link ;
    private int delete_state;

    public UsersDAO(SQLiteDatabase sqLiteDatabase) {

        super(sqLiteDatabase);
    }

    public void add_user(Users user){
        Log.i("add_user", "cocuc + " + user.getUser_phone_number());
        ContentValues values = new ContentValues();
        values.put("user_phone_number", user.getUser_phone_number());
        long inserResult = db.insert(table_name,null,values);
        Log.i("table_nameinse", "table_nameinse " + inserResult);
    }

    public boolean change_user(Users usr){

        return true;
    }

    public boolean delete_user(Users usr){

        return true;
    }

    public void search_user(SQLiteDatabase database){
    }

    public List<Users> getAllUsers(){

        Log.i("getAllUsers", "getAllUsers");
        String query =  "SELECT * FROM "+ table_name;
        List<Users> usersList = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                Users user = new Users();
                user.setId_user(cursor.getInt(0));
                usersList.add(user);
            }
            while (cursor.moveToNext());
        }
        db.close();
        Log.i("getAllUsersEnd", "getAllUsersEnd");
        return usersList;
    }
}
