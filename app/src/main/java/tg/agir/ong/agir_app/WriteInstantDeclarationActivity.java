package tg.agir.ong.agir_app;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WriteInstantDeclarationActivity extends Activity {
    private LocationManager lm;
    private LocationListener ll;
    private Button send_instant_write_declaration;
    private EditText instant_write_declaration_edit_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_instant_declaration);

        send_instant_write_declaration = (Button) findViewById(R.id.send_instant_write_declaration);
        instant_write_declaration_edit_text = (EditText) findViewById(R.id.instant_write_declaration_edit_text);

        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        ll = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                instant_write_declaration_edit_text.setText(""+location.getLatitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


        send_instant_write_declaration.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if(ActivityCompat.checkSelfPermission(WriteInstantDeclarationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(WriteInstantDeclarationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                    if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) == true && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) == true)
                    {
                        instant_write_declaration_edit_text.setText("Si la personne a une fois accepté mais a desactivé apres");
                    }
                    else
                    {
                        requestPermissions(new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

                    }

                }
                else{
                    lm.requestLocationUpdates("gps",1000, 0, ll);

                }


            }
        });



    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if(requestCode == 1)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {

            }
            else
            {

            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
