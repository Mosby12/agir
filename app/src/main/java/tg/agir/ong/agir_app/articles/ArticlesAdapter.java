package tg.agir.ong.agir_app.articles;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.List;

import tg.agir.ong.agir_app.R;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.MyViewHolder> {

    private List<ArticlesDataModel> dataModelList;
    private Context mContext;

    // View holder class whose objects represent each list item
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView cardImageView;
        public TextView titleTextView;
        public TextView contentTextView;
        public Button readMoreButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cardImageView = itemView.findViewById(R.id.article_image_view);
            titleTextView = itemView.findViewById(R.id.article_title);
            contentTextView = itemView.findViewById(R.id.article_content);
            readMoreButton = itemView.findViewById(R.id.article_read_more_action_button);
        }

        public void bindData(ArticlesDataModel dataModel, Context context) {
            cardImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.article_default_image));
            titleTextView.setText(dataModel.getTitle());
            contentTextView.setText(Html.fromHtml(dataModel.getContent()));
            //Log.i("picasso", "" + dataModel.getBannerUrl());
            Picasso.get().load(dataModel.getBannerUrl()).into(cardImageView);

            final Context finalContext = context;
            final ArticlesDataModel finalDataModel = dataModel;
            readMoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.i("picasso", "" + finalDataModel.getUrl());
                    finalContext.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(finalDataModel.getUrl())));
                }
            });
        }
    }

    public ArticlesAdapter(List<ArticlesDataModel> modelList, Context context) {
        dataModelList = modelList;
        mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate out card list item

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_articles, parent, false);
        // Return a new view holder

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Bind data for the item at position

        holder.bindData(dataModelList.get(position), mContext);
    }

    @Override
    public int getItemCount() {
        // Return the total number of items

        return dataModelList.size();
    }
}