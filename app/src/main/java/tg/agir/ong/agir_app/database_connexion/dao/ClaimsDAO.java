package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import tg.agir.ong.agir_app.entities.Claims;

public class ClaimsDAO extends DAO<Claims> {
    //Table name
    private static final String table_name = "claims";

    //Table columns
    private static final String column_id_claim = "id_claim";
    private static final String column_declaim = "declaim" ;
    private static final String column_date_claim_occur = "date_claim_occur";
    private static final String column_audio_content = "audio_content";
    private static final String column_video_content= "video_content";
    private static final String column_text_content = "text_content";
    private static final String column_place_longitude = "place_longitude" ;
    private static final String column_place_latitude = "place_latitude";
    private static final String column_saved_on = "saved_on";
    private static final String column_claim_status = "claim_status";
    private static final String column_delete_state = "delete_state";

    public ClaimsDAO(SQLiteDatabase pcon) {
        super(pcon);
    }

    public void add_claims(Claims claim){
        ContentValues values = new ContentValues();
        values.put(column_declaim,claim.getDeclaim());
        values.put(column_date_claim_occur,claim.getDate_claim_occur());
        values.put(column_audio_content,claim.getAudio_content());
        values.put(column_video_content,claim.getVideo_content());
        values.put(column_text_content,claim.getText_content());
        values.put(column_place_longitude,claim.getPlace_longitude());
        values.put(column_place_latitude,claim.getPlace_latitude());
        values.put(column_saved_on,claim.getSaved_on());
        values.put(column_claim_status,claim.getClaim_status());
        values.put(column_delete_state,claim.getDelete_state());
        db.insert(table_name, null, values);
    }


    public Claims getSpecifyClaim(int i){
        Claims claim = new Claims();
        String query =  "SELECT * FROM "+ table_name +" WHERE id_claim ="+ i ;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                claim.setId_claim(cursor.getInt(0));
                claim.setDeclaim(cursor.getInt(1));
                claim.setDate_claim_occur(cursor.getString(2));
                claim.setAudio_content(cursor.getString(3));
                claim.setVideo_content(cursor.getString(4));
                claim.setText_content(cursor.getString(5));
                claim.setPlace_longitude(cursor.getDouble(6));
                claim.setPlace_latitude(cursor.getDouble(7));
                claim.setSaved_on(cursor.getString(8));
                claim.setClaim_status(cursor.getInt(9));
                claim.setDelete_state(cursor.getInt(10));
            }
            while (cursor.moveToNext());
        }
        return claim;
    }

    public ArrayList<Claims> getAllClaiims(){
        String query =  "SELECT * FROM "+ table_name;
        ArrayList<Claims> claimsList = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                Claims claim = new Claims();
                claim.setId_claim(cursor.getInt(0));
                claim.setDeclaim(cursor.getInt(1));
                claim.setDate_claim_occur(cursor.getString(2));
                claim.setAudio_content(cursor.getString(3));
                claim.setVideo_content(cursor.getString(4));
                claim.setText_content(cursor.getString(5));
                claim.setPlace_longitude(cursor.getDouble(6));
                claim.setPlace_latitude(cursor.getDouble(7));
                claim.setSaved_on(cursor.getString(8));
                claim.setClaim_status(cursor.getInt(9));
                claim.setDelete_state(cursor.getInt(10));
                claimsList.add(claim);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return claimsList;
    }
}
