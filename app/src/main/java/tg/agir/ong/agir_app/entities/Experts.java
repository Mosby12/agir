package tg.agir.ong.agir_app.entities;

public class Experts {
    private int id_expert;
    private String expert_lastname;
    private String expert_firstname;
    private String expert_photo;
    private String expert_email;
    private int expert_type;
    private String expert_contact1;
    private String expert_contact2;
    private int delete_state;

    public Experts() {

    }

    public int getId_expert() {
        return id_expert;
    }

    public void setId_expert(int id_expert) {
        this.id_expert = id_expert;
    }

    public String getExpert_lastname() {
        return expert_lastname;
    }

    public void setExpert_lastname(String expert_lastname) {
        this.expert_lastname = expert_lastname;
    }

    public String getExpert_firstname() {
        return expert_firstname;
    }

    public void setExpert_firstname(String expert_firstname) {
        this.expert_firstname = expert_firstname;
    }

    public String getExpert_photo() {
        return expert_photo;
    }

    public void setExpert_photo(String expert_photo) {
        this.expert_photo = expert_photo;
    }

    public String getExpert_email() {
        return expert_email;
    }

    public void setExpert_email(String expert_email) {
        this.expert_email = expert_email;
    }

    public int getExpert_type() {
        return expert_type;
    }

    public void setExpert_type(int expert_type) {
        this.expert_type = expert_type;
    }

    public String getExpert_contact1() {
        return expert_contact1;
    }

    public void setExpert_contact1(String expert_contact1) {
        this.expert_contact1 = expert_contact1;
    }

    public String getExpert_contact2() {
        return expert_contact2;
    }

    public void setExpert_contact2(String expert_contact2) {
        this.expert_contact2 = expert_contact2;
    }

    public int getDelete_state() {
        return delete_state;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }
}