package tg.agir.ong.agir_app;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;

public class FirebaseMessagingIDService extends FirebaseMessagingService {
    private String fireBaseToken;

    @Override
    public void onNewToken(String s) {
        Log.i("onNewToken", "enter");
        super.onNewToken(s);
        fireBaseToken = s;
    }

    public String getFireBaseToken() {

        Log.i("getFireBaseToken", "enter");
        return fireBaseToken;
    }
}
