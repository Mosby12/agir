package tg.agir.ong.agir_app;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;

import androidx.appcompat.app.AppCompatActivity;
import tg.agir.ong.agir_app.util.Util;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        CircularFillableLoaders circularFillableLoaders = (CircularFillableLoaders) findViewById(R.id.circularFillableLoaders);

        new CircularFillableLoadersAsyncTask(circularFillableLoaders).execute();

// Set Wave and Border Color
// Set Border Width
        //   circularImageView.setBorderWidth(10 * getResources().getDisplayMetrics().density);
// Set Wave Amplitude (between 0.00f and 0.10f)
        //switch from splash activity to main activity

    }

    private class CircularFillableLoadersAsyncTask extends AsyncTask<Void, Integer, Void> {
        private CircularFillableLoaders mCircularFillableLoaders;

        public CircularFillableLoadersAsyncTask(CircularFillableLoaders circularFillableLoaders) {
            mCircularFillableLoaders = circularFillableLoaders;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            updateProgress(progress);
            mCircularFillableLoaders.setProgress(progress);
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int progress = 0; progress < 100; progress++) {
                try {
                    Thread.sleep(50);
                    publishProgress(progress);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            launchActivity();


            return null;
        }
    }

    public void launchActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void updateProgress(int k){
        TextView textView = (TextView) findViewById(R.id.loading);
        textView.setText("Nous chargeons vos données "+k+"%");

    }
}
