package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import tg.agir.ong.agir_app.entities.Chat;

public class ChatDAO extends DAO<Chat>{
    private static final String table_name="chat";

    private static final String column_type_receiver="type_receiver";
    private static final String column_content_text="content_text";
    private static final String column_content_audio="content_audio";
    private static final String column_content_video="content_video";
    private static final String column_saved_on="saved_on";
    private static final  String column_saved_by="saved_by";
    private static final  String column_modified_on="modified_on";
    private static final  String column_modified_by="modified_by";
    //send =1 , receive=2, read=3, delete=3="";
    private  static final  String column_delete_state="delete_state";


    public ChatDAO(SQLiteDatabase pcon) {
        super(pcon);

    }

    private static String Query;

    public void addmessage(){
        String query =" INSERT INTO chat(id_chat, id_sender, type_sender, id_receiver, type_receiver, content_text, content_audio, content_video, saved_on, saved_by, modified_on, modified_by, delete_state) VALUES(,1,1,1,1,,,,,,,,,)";

        ContentValues values = new ContentValues();
        values.put("id_sender",1);
        db.insert(table_name,null,values);
    }

    public static List<Chat> get_All_Expert(){

        Query=  "SELECT * FROM chat";
        List<Chat> lstExpert = new ArrayList<>();
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.moveToFirst()){
            do {
                Chat cht = new Chat();
                cht.setId_chat(cursor.getInt(0));
                lstExpert.add(cht);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return lstExpert;
    }
    public List<Chat> get_All_Folder(){

        Query=  "SELECT * FROM chat";
        List<Chat> lstExpert = new ArrayList<>();
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.moveToFirst()){
            do {
                Chat cht = new Chat();
                cht.setId_chat(cursor.getInt(0));
                lstExpert.add(cht);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return lstExpert;
    }
    public List<Chat> get_All_tchat(){

        Query=  "SELECT * FROM chat";
        List<Chat> lstExpert = new ArrayList<>();
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.moveToFirst()){
            do {
                Chat cht = new Chat();
                cht.setId_chat(cursor.getInt(0));
                lstExpert.add(cht);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return lstExpert;
    }
}
