package tg.agir.ong.agir_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import tg.agir.ong.agir_app.fragment.FragmentCenters;
import tg.agir.ong.agir_app.fragment.FragmentInstantDeclaration;
import tg.agir.ong.agir_app.fragment.FragmentSensitization;
import tg.agir.ong.agir_app.fragment.InstantDeclarationFragment;
import tg.agir.ong.agir_app.util.Util;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

public class AllInstantDeclarationMenuActivity extends AppCompatActivity {

    private AppBarLayout appBarLayout;
    private TabLayout tablayout1;
    private ViewPager viewPager1;

    private InstantDeclarationFragment frament_instant_declaration;
    private FragmentCenters fragment_centrals;
    private FragmentSensitization frangment_sensitization;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_instant_declaration_menu_activity);

        init();
    }

    public void init(){
        frament_instant_declaration = new InstantDeclarationFragment();
        fragment_centrals = new FragmentCenters();
        frangment_sensitization = new FragmentSensitization();
        appBarLayout =  findViewById(R.id.applicationbar_id);

        tablayout1 =  findViewById(R.id.declaration_menu_instant_tablayout_id);
        viewPager1 =  findViewById(R.id.declaration_menu_instant_viewpager_id);
        ViewPagerAdapter adapter1 = new ViewPagerAdapter(getSupportFragmentManager());

        adapter1.addFragment(frament_instant_declaration,"DECLARATION");
        adapter1.addFragment(frangment_sensitization,"NOUVELLES");
        adapter1.addFragment(fragment_centrals,"CENTRES");

        viewPager1.setAdapter(adapter1);
        tablayout1.setupWithViewPager(viewPager1);
    }





}
