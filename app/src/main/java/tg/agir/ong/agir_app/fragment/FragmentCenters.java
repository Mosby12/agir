package tg.agir.ong.agir_app.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;

import java.util.Map;

import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WebSocketClientManager;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;

public class FragmentCenters extends Fragment {
    View view;
    MapView map = null;
    private Connection dbConnection;
    private WebSocketClientManager webSocketClientManager;
    private RotationGestureOverlay mRotationGestureOverlay;

    public FragmentCenters() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.centers_fragment, container, false);

        map = (MapView) view.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(20.0);
        GeoPoint startPoint = new GeoPoint(35.155688, 136.926644);
        mapController.setCenter(startPoint);


        mRotationGestureOverlay = new RotationGestureOverlay(view.getContext(), map);
        mRotationGestureOverlay.setEnabled(true);
        map.setMultiTouchControls(true);
        map.getOverlays().add(this.mRotationGestureOverlay);


        Marker startMarker = new Marker(map);
        startMarker.setPosition(startPoint);
        startMarker.setIcon(getResources().getDrawable(R.drawable.marker_default));
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        startMarker.setTitle("Start point");

        map.getOverlays().add(startMarker);

        map.invalidate();

        /*GroundOverlay2 overlay = new GroundOverlay2();
        overlay.setTransparency(0.5f);
        overlay.setImage(new Bitmap());
        overlay.setPosition(new GeoPoint(35.264688, 136.921644), new GeoPoint( 35.251688, 136.926244));
        map.getOverlayManager().add(overlay);*/


        // Reading list of screening centers in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        String screeningCentersList = preferences.getString("globalScreeningCentersList", "");

        if (!screeningCentersList.equals("")) {
            String formattedScreeningCentersList = screeningCentersList.replace("_FIRST_POINT_", ":");
            Log.i("customI5424", "" + formattedScreeningCentersList);
            final Map<String, String> screeningCentersDataMap = new Gson().fromJson(formattedScreeningCentersList, Map.class);
            Log.i("customIlast", "enter");

            /*
            CustomMapOverLay mapOverlay = null;
            Drawable marker=getResources().getDrawable(R.drawable.marker_default);
            int markerWidth = marker.getIntrinsicWidth();
            int markerHeight = marker.getIntrinsicHeight();
            marker.setBounds(0, markerHeight, markerWidth, 0);

            mapOverlay = new CustomMapOverLay(marker);
            map.getOverlays().add(mapOverlay);

            GeoPoint point = new GeoPoint(55.75, 37.616667);
            mapOverlay.addItem(point, "Russia", "Russia");
            */


            for (int i = 0; i < screeningCentersDataMap.size(); i++) {

                String subFormattedScreeningCentersList = screeningCentersDataMap.get("" + i).
                        replace("_SECOND_POINT_", "\":\"");
                subFormattedScreeningCentersList = subFormattedScreeningCentersList.replace("_OPEN_ACCOLADE_", "{\"");
                subFormattedScreeningCentersList = subFormattedScreeningCentersList.replace("_COMMA_", "\",\"");
                subFormattedScreeningCentersList = subFormattedScreeningCentersList.replace("_CLOSE_ACCOLADE_", "\"}");
                Log.i("customIsecon", "" + subFormattedScreeningCentersList);
                final Map<String, String> screeningCentersSubDataMap = new Gson().fromJson(subFormattedScreeningCentersList, Map.class);

                String centerName = screeningCentersSubDataMap.get("name").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
                String centerDescription = screeningCentersSubDataMap.get("description").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
                String centerEmail = screeningCentersSubDataMap.get("email").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
                String centerPhoneNumber = screeningCentersSubDataMap.get("phone_number").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
                String centerLatitude = screeningCentersSubDataMap.get("latitude").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
                String centerLongitude = screeningCentersSubDataMap.get("longitude").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
                String centerPhoto = screeningCentersSubDataMap.get("photo").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");

                Marker screeningCenterMarker = new Marker(map);
                screeningCenterMarker.setPosition(new GeoPoint(Double.parseDouble(centerLatitude), Double.parseDouble(centerLongitude)));
                screeningCenterMarker.setIcon(getResources().getDrawable(R.drawable.marker_default));
                screeningCenterMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                String popupContent = getResources().getString(R.string.map_marker_popup_content_format);
                popupContent = popupContent.replace("_NAME_", centerName);
                popupContent = popupContent.replace("_DESC_", centerDescription);
                popupContent = popupContent.replace("_TEL_", centerPhoneNumber);
                popupContent = popupContent.replace("_EMAIL_", centerEmail);
                screeningCenterMarker.setTitle(popupContent);
                map.getOverlays().add(screeningCenterMarker);

            }
            //Log.i("customI2222", "" + screeningCentersDataMap.get("name"));

/*
            // create 10k labelled points
// in most cases, there will be no problems of displaying >100k points, feel free to try
            List<IGeoPoint> points = new ArrayList<>();
            for (int i = 0; i < 10000; i++) {
                points.add(new LabelledGeoPoint(35 + Math.random() * 5, 136 + Math.random() * 5
                        , "Point #" + i));
            }

// wrap them in a theme
            SimplePointTheme pt = new SimplePointTheme(points, true);

// create label style
            Paint textStyle = new Paint();
            textStyle.setStyle(Paint.Style.FILL);
            textStyle.setColor(Color.parseColor("#0000ff"));
            textStyle.setTextAlign(Paint.Align.CENTER);
            textStyle.setTextSize(24);

// set some visual options for the overlay
// we use here MAXIMUM_OPTIMIZATION algorithm, which works well with >100k points
            SimpleFastPointOverlayOptions opt = SimpleFastPointOverlayOptions.getDefaultStyle()
                    .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.MAXIMUM_OPTIMIZATION)
                    .setRadius(7).setIsClickable(true).setCellSize(15).setTextStyle(textStyle);

// create the overlay with the theme
            final SimpleFastPointOverlay sfpo = new SimpleFastPointOverlay(pt, opt);

// onClick callback
            sfpo.setOnClickListener(new SimpleFastPointOverlay.OnClickListener() {
                @Override
                public void onClick(SimpleFastPointOverlay.PointAdapter points, Integer point) {
                    Toast.makeText(map.getContext()
                            , "You clicked " + ((LabelledGeoPoint) points.get(point)).getLabel()
                            , Toast.LENGTH_SHORT).show();
                }
            });

// add overlay
            map.getOverlays().add(sfpo);
            */


        }


        //webSocketClientManager = getArguments().getInt(EXTRA_TITLE);
        //dbConnection = getArguments().getSerializable("dbConnection").getClass().;
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        Log.i("onHiddenChangedf", (hidden + " is on screen"));
        super.onHiddenChanged(hidden);
        if (hidden) {
            Log.i("getSimpleName", ((Object) this).getClass().getSimpleName() + " is NOT on screen");
        } else {
            Log.i("getSimpleName", ((Object) this).getClass().getSimpleName() + " is on screen");
        }
    }

    public void onPause() {
        super.onPause();
        Log.i("onPause", (" is on screen"));
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("onResume", (" is on screen"));
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("onStop", (" is on screen"));
    }

}
