package tg.agir.ong.agir_app.database_connexion.dao;

import android.database.sqlite.SQLiteDatabase;

import tg.agir.ong.agir_app.entities.Folder;

public class FolderDAO extends DAO<Folder>{
    public FolderDAO(SQLiteDatabase pcon) {
        super(pcon);
    }
}
