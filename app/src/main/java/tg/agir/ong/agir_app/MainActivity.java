package tg.agir.ong.agir_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.hbb20.CountryCodePicker;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import tg.agir.ong.agir_app.database_connexion.dao.Connection;

public class MainActivity extends Activity {
    private Connection dbConnection;
    private EditText e;
    private CountryCodePicker countryCodePicker;
    private EditText phoneNumberEditText;
    private Button checkPhoneNumberButton;
    private TextView incorrectPhoneNumberLabel;
    private String phoneNumberValue;
    private String countryCodeValue;
    private WebSocketClientManager webSocketClientManager;
    private String fireBaseToken;
    private SMSListener smsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setTitle(getResources().getString(R.string.app_name);
        setContentView(R.layout.activity_main);

        // Reading user phone number in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        String phoneNumber = preferences.getString("globalPhoneNumber", "");

        /* Delete immediately after test */
         //phoneNumber = "+22893604126";


        //if (false) {
        if (!phoneNumber.equals("")) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            this.startActivity(intent);
            finish();
        } else {


            /*dbConnection = new Connection(this);
            Users user = new Users();
            user.setUser_phone_number("+855422444");
            //Users usr = new Users("jsatchivi@gmail.com", "james");
            Log.i("dbConnection", "" +  dbConnection.getDb());
            Log.i("dbConnectionisReadOnly", "" +  dbConnection.getDb().isReadOnly());
            DAO<Users> usersDAO = new UsersDAO(dbConnection.getDb());
            //((UsersDAO) duser).add_user(usr, dbConnection.getWritableDatabase());
            ((UsersDAO) usersDAO).add_user(user);


            List<Users> usersList = ((UsersDAO) usersDAO).getAllUsers();
            Log.i("usersListasize", "" +  usersList.size());

            for (int i = 0; i < usersList.size(); i++){

                Log.i("usersListWelc", "" + usersList.get(i).getUser_phone_number());

            }*/

            /*DAO<Users> dbuser = new UsersDAO(dbConnection.getDb());
            Users u = ((UsersDAO) dbuser).search_user(dbConnection.getReadableDatabase());
            e = (EditText) findViewById(R.id.edit_text_enter_phone);
            e.setText(u.getUser_email());*/


            // Open the connection with the server
            webSocketClientManager = new WebSocketClientManager(MainActivity.this, dbConnection);
            webSocketClientManager.connectWebSocket();


            // Bind SMS listener to read incoming SMS
            this.bindSMSListener();

            // Getting FireBase token after registration
            this.getFireBaseToken();

            // Country code picker biding from layout
            countryCodePicker = (CountryCodePicker) findViewById(R.id.spinner_choose_country);

            // Phone number biding from layout
            phoneNumberEditText = (EditText) findViewById(R.id.edit_text_enter_phone);

            // Check Phone Number Button biding from layout
            checkPhoneNumberButton = (Button) findViewById(R.id.button_check_phone_number);

            // Incorrect Phone Number Label TextView biding from layout
            incorrectPhoneNumberLabel = (TextView) findViewById(R.id.text_view_label_error_enter_correct_phone_number);

            // Attaching the phone number editText to the country code picker.
            countryCodePicker.registerCarrierNumberEditText(phoneNumberEditText);

            // Validation of entered full-number
            countryCodePicker.isValidFullNumber();

            // Number Validity Change Listener
            countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
                @Override
                public void onValidityChanged(boolean isValidNumber) {
                    Log.i("onValidityChanged", "" + isValidNumber);
                    // Checking if the phone number edit text is empty or not
                    if (phoneNumberEditText.getText().toString().trim() != "") {
                        Log.i("onValidityChangedEnter", ">" + phoneNumberEditText.getText().toString().trim() + "<");
                        if (isValidNumber) {
                            incorrectPhoneNumberLabel.setVisibility(View.INVISIBLE);
                        } else {
                            //incorrectPhoneNumberLabel.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            // Country code picker change listener
            countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected() {
                    countryCodeValue = countryCodePicker.getSelectedCountryCodeWithPlus();
                    // Toast.makeText(getContext(), "Updated " + ccp.getSelectedCountryName(), Toast.LENGTH_SHORT).show();
                }
            });

            // Country code picker dialog events listener
            countryCodePicker.setDialogEventsListener(new CountryCodePicker.DialogEventsListener() {
                @Override
                public void onCcpDialogOpen(Dialog dialog) {
                    //your code
                }

                @Override
                public void onCcpDialogDismiss(DialogInterface dialogInterface) {
                    //your code
                }

                @Override
                public void onCcpDialogCancel(DialogInterface dialogInterface) {
                    //your code
                }
            });

            // Adding click event listener to the check phone number button
            checkPhoneNumberButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if (webSocketClientManager.isConnectedToServer()) {
                    if (!webSocketClientManager.isConnectedToServer()) {
                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                //set title
                                .setTitle(MainActivity.this.getResources().getString(R.string.general_error_message))
                                //set message
                                .setMessage(MainActivity.this.getResources().getString(R.string.login_error_cannot_connect_to_server))
                                //set positive button
                                .setPositiveButton(MainActivity.this.getResources().getString(R.string.general_retry_message), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        // Trying to connect again to server
                                        webSocketClientManager.connectWebSocket();
                                    }
                                })
                                //set negative button
                                .setNegativeButton(MainActivity.this.getResources().getString(R.string.general_cancel_message), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //set what should happen when negative button is clicked
                                        //Toast.makeText(getApplicationContext(),"Nothing Happened",Toast.LENGTH_LONG).show();
                                    }
                                })
                                .show();
                    } else {
                        // Getting the value of the selected country code
                        countryCodeValue = countryCodePicker.getSelectedCountryCodeWithPlus();
                        // Getting the value of the phone number
                        phoneNumberValue = countryCodePicker.getFullNumberWithPlus();


                        // Checking if the country code and phone number exist
                        if (countryCodeValue != null && countryCodeValue != ""
                                && phoneNumberValue != null && phoneNumberValue != ""
                                && phoneNumberValue.length() > 5) {
                            Log.i("setOnClickListener", phoneNumberValue);
                            // Hide the error label
                            incorrectPhoneNumberLabel.setVisibility(View.INVISIBLE);
                            // Get app info (lang, app version,...)
                            String appInfo = MainActivity.this.getAppInfo();
                            // Get device info (manufacturer, os version,...)
                            String deviceInfo = MainActivity.this.getDeviceInfo();
                            // Get the formatted number without country code
                            //String shortNumber = phoneNumberValue.replace(countryCodeValue,"");
                            // Creating a map to contain the data to send to server
                            Map<String, String> dataToSend = new HashMap<String, String>();

                            // Put elements to the data
                            //dataToSend.put("codeCountry", countryCodeValue);
                            dataToSend.put("\"phoneNumber\"", "\"" + phoneNumberValue + "\"");
                            dataToSend.put("\"appInfo\"", "\"" + appInfo + "\"");
                            dataToSend.put("\"deviceInfo\"", "\"" + deviceInfo + "\"");
                            //dataToSend.put("fireBaseToken", fireBaseToken);

                            // Send a sign in request to the server
                            webSocketClientManager.sendSignInMessage(dataToSend);

                        } else {
                            incorrectPhoneNumberLabel.setVisibility(View.VISIBLE);
                        }
                    }

                }
            });

        }

    }

    public void getFireBaseToken() {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            //Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        fireBaseToken = task.getResult().getToken();

                    }
                });

    }

    public String getDeviceInfo() {
        // Getting all info from the device
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        String ScreenResolution_value = "'SCREEN':'" + width + "x" + height + "', ";
        String ManufacturerValue = "'MANUFACTURER':'" + Build.MANUFACTURER + "', ";
        String Brand_value = "'BRAND':'" + Build.BRAND + "', ";
        String Model_value = "'MODEL':'" + Build.MODEL + "', ";
        String Board_value = "'BOARD':'" + Build.BOARD + "', ";
        String Hardware_value = "'HARDWARE':'" + Build.HARDWARE + "', ";
        String Serial_nO_value = "'SERIAL':'" + Build.SERIAL + "', ";
        String BootLoader_value = "'BOOTLOADER':'" + Build.BOOTLOADER + "', ";
        String User_value = "'USER':'" + Build.USER + "', ";
        String Host_value = "'HOST':'" + Build.HOST + "', ";
        String Version = "'RELEASE':'" + Build.VERSION.RELEASE + "', ";
        String API_level = "'SDK_INT':'" + Build.VERSION.SDK_INT + "', ";
        String Build_ID = "'ID':'" + Build.ID + "', ";
        String Build_Time = "'TIME':'" + Build.TIME + "', ";
        String Fingerprint = "'FINGERPRINT':'" + Build.FINGERPRINT + "', ";
        String android_id =
                "'ANDROID_ID':'" + Settings.Secure.getString(MainActivity.this.getContentResolver(),
                        Settings.Secure.ANDROID_ID) + "', ";
        String fireBaseTokenValue = "'FIREBASE_TOKEN':'" + fireBaseToken + "'";

        String deviceInfo = "{" + android_id + API_level + Board_value + BootLoader_value
                + Brand_value + Build_ID + Build_Time + Fingerprint + Hardware_value
                + Host_value + ManufacturerValue + Model_value + ScreenResolution_value
                + Serial_nO_value + User_value + Version + fireBaseTokenValue + "}";

        return deviceInfo;

    }

    public String getAppInfo() {
        // Getting all info from the app
        Locale currentLocale = getResources().getConfiguration().locale;
        String currentLocaleString = "'LANG':'" + currentLocale.getLanguage() + "'";
        //String currentLocaleString = "LANG=" + currentLocale.getLanguage() + ", ";

        String appInfo = "{" + currentLocaleString + "}";

        return appInfo;
    }

    public void bindSMSListener() {
        // Check if SMS reading permission is granted
        if (!PermissionsTool.Check_READ_SMS(MainActivity.this)) {
            //if not permission granted so request permission with request code
            PermissionsTool.Request_READ_SMS(MainActivity.this, 22);
        }
        //if(PermissionsTool.Check_READ_SMS(MainActivity.this)) {
        Log.i("smsListener", "enter");
        smsListener = new SMSListener();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        MainActivity.this.registerReceiver(smsListener, filter);
        Log.i("smsListenerEnd", "end");
        //}
    }

    public void unbindSMSListener() {
        MainActivity.this.unregisterReceiver(smsListener);
    }
}