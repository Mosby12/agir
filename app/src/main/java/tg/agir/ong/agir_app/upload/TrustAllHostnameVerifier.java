package tg.agir.ong.agir_app.upload;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

class TrustAllHostnameVerifier implements HostnameVerifier {
    public boolean verify(String hostname, SSLSession session) { return true; }
}
