package tg.agir.ong.agir_app.chat;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Varun John on 4 Dec, 2018
 * Github : https://github.com/varunjohn
 */
public class Message implements Serializable {

    public static int TYPE_TEXT = 1;
    public static int TYPE_AUDIO = 21;


    public String text;
    public int type;
    public int time;
    public  boolean is_sender=true;
    public String id_claim="";
    public String audio_path="";

    public Message(String text) {
        this.text = text;
        this.type = TYPE_TEXT;
    }

    public Message(int time) {
        this.time = time;
        this.type = TYPE_AUDIO;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isSender() {
        return is_sender;
    }

    public void setIs_sender(boolean is_sender) {
        this.is_sender = is_sender;
    }

    public String getId_claim() {
        return id_claim;
    }

    public void setId_claim(String id_claim) {
        this.id_claim = id_claim;
    }

    public void setAudio_path(String audio_path) {
        this.audio_path = audio_path;
    }

    public String getAudio_path() {
        return audio_path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return type == message.type &&
                time == message.time &&
                is_sender == message.is_sender &&
                Objects.equals(text, message.text) &&
                Objects.equals(id_claim, message.id_claim) &&
                Objects.equals(audio_path, message.audio_path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, type, time, is_sender, id_claim, audio_path);
    }
}
