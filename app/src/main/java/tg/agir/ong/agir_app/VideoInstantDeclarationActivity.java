package tg.agir.ong.agir_app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import tg.agir.ong.agir_app.database_connexion.dao.ClaimsDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.entities.Claims;
import tg.agir.ong.agir_app.entities.District;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class VideoInstantDeclarationActivity extends AppCompatActivity {
    private Connection con;
    private Claims claimtosave;
    private List<Claims> claimsList;

    VideoView videoView;
    private ImageButton take_instant_video;
    private ImageButton send_media_button;
    private Uri video_uri;
    private static int VIDEO_REQUEST = 101;
    private MediaController mediaController;
    Uri videoFileUri;

    Calendar calendar;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_instant_declaration);
        init();
        action_onclick();
    }

    public void init(){
        con = new Connection(this);
        take_instant_video = findViewById(R.id.take_video_instant_button);
        send_media_button = findViewById(R.id.save_media_instant_button);
        videoView = findViewById(R.id.video_instant_videoview);
        mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
        claimtosave = new Claims();
    }

    public void action_onclick(){
        take_instant_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPermissionFromDevice()){
                    requestPermission();
                }else {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    video_uri = getPath();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, video_uri);
                    intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                    startActivityForResult(intent, VIDEO_REQUEST);
                }
            }
        });

        send_media_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(video_uri == null ){
                    Toast.makeText(VideoInstantDeclarationActivity.this, "Completer les champs", Toast.LENGTH_SHORT).show();
                }else {
                    final DAO<Claims> claimdao = new ClaimsDAO(con.getDb());
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    String currentDateTimeString = dateFormat.format(cal.getTime());
                    claimtosave.setVideo_content(video_uri.getPath());
                    claimtosave.setDeclaim(1); // Change it with the user id
                    claimtosave.setSaved_on(currentDateTimeString);
                    claimtosave.setClaim_status(1);
                    claimtosave.setDelete_state(0);
                    try {
                        ((ClaimsDAO) claimdao).add_claims(claimtosave);
                        Toast.makeText(VideoInstantDeclarationActivity.this, "ok", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(VideoInstantDeclarationActivity.this, "non", Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(VideoInstantDeclarationActivity.this, "Enregistrement réussi", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == VIDEO_REQUEST){
            if (resultCode == RESULT_OK){
                Toast.makeText(this, "ca marche", Toast.LENGTH_SHORT).show();
                videoView.setVideoURI(video_uri);
                videoView.setBackground(null);
                videoView.start();

            }
            else{
                Toast.makeText(this, "ca marche pas", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){

            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    requestPermission();
                }
            }
            break;
        }
    }

    private boolean checkPermissionFromDevice() {
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int take_photo_result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                read_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                take_photo_result == PackageManager.PERMISSION_GRANTED;
    }

    public void saveImageToGallery(Bitmap bitmap){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root+"/nol");

    }

    public boolean isExternalStorageWritable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            Log.i("writeveri","is OK");
            return true;
        }
        else {
            return false;
        }
    }

    public Uri getPath(){
        File video_file = null;
        Uri apkURI = null;
        if(isExternalStorageWritable()) {
            File folder = new File(Environment.getExternalStorageDirectory() + "/ong_agir/video");

            if (!folder.exists()) {
                folder.mkdirs();
            }
            video_file = new File(folder, UUID.randomUUID().toString()+"claim_video.mp4");
            apkURI = FileProvider.getUriForFile(
                    this,
                    this.getApplicationContext()
                            .getPackageName() + ".provider", video_file);

        }
        return apkURI;
    }
}
