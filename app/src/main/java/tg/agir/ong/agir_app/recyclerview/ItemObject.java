package tg.agir.ong.agir_app.recyclerview;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tg.agir.ong.agir_app.chat.Message;

public class ItemObject implements Comparable<ItemObject> {

    private String name = "";
    private String textContent = "";
    private int photoId;
    private Date date = new Date();
    private String text = "";
    private String location = "";
    private String audio_path = "";
    private String video_path = "";
    private String id_claim = "";
    private int count_new_message=0;
    private List<Message> messageList = new ArrayList<>();

    public ItemObject() {
        count_new_message = 0;
        messageList = new ArrayList<>();
    }

    public ItemObject(String name, String textContent, int photoId, Date date) {
        this.name = name;
        this.textContent = textContent;
        this.photoId = photoId;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public Date getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public String getLocation() {
        return location;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int compareTo(ItemObject o) {
        return getDate().compareTo(o.getDate());
    }

    public String getAudio_path() {
        return audio_path;
    }

    public void setAudio_path(String audio_path) {
        this.audio_path = audio_path;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getFormattedDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(this.date);
    }

    public String getId_claim() {
        return id_claim;
    }

    public void setId_claim(String id_claim) {
        this.id_claim = id_claim;
    }

    public int getCount_new_message() {
        return count_new_message;
    }

    public void setCount_new_message(int count_new_message) {
        this.count_new_message = count_new_message;
    }

    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
}
