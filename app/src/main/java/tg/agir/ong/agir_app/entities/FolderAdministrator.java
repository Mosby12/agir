package tg.agir.ong.agir_app.entities;

public class FolderAdministrator {

    private int id_folder_administrator;
    private int id_folder;
    private int id_expert ;
    private int saved_by;
    private String saved_on;
    private int modified_by;
    private int modified_on;
    // 1=Assigned, 2= Removed;
    private int delete_state;

    public void setId_folder_administrator(int id_folder_administrator) {
        this.id_folder_administrator = id_folder_administrator;
    }

    public void setId_folder(int id_folder) {
        this.id_folder = id_folder;
    }

    public void setExpert(int expert) {
        this.id_expert = expert;
    }

    public void setSaved_by(int saved_by) {
        this.saved_by = saved_by;
    }

    public void setSaved_on(String saved_on) {
        this.saved_on = saved_on;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public void setModified_on(int modified_on) {
        this.modified_on = modified_on;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }

    public int getId_folder_administrator() {
        return id_folder_administrator;
    }

    public int getId_folder() {
        return id_folder;
    }

    public int getExpert() {
        return id_expert;
    }

    public int getSaved_by() {
        return saved_by;
    }

    public String getSaved_on() {
        return saved_on;
    }

    public int getModified_by() {
        return modified_by;
    }

    public int getModified_on() {
        return modified_on;
    }

    public int getDelete_state() {
        return delete_state;
    }



}
