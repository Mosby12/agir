package tg.agir.ong.agir_app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import tg.agir.ong.agir_app.database_connexion.dao.CitiesDAO;
import tg.agir.ong.agir_app.database_connexion.dao.ClaimsDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.database_connexion.dao.DistrictDAO;
import tg.agir.ong.agir_app.database_connexion.dao.UsersDAO;
import tg.agir.ong.agir_app.entities.Cities;
import tg.agir.ong.agir_app.entities.Claims;
import tg.agir.ong.agir_app.entities.District;
import tg.agir.ong.agir_app.entities.Users;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class VideoLaterDeclarationActivity extends AppCompatActivity {
    private Connection con;
    private Claims claimtosave;
    private List<Claims> claimsList;

    VideoView videoView;
    private TextView video_later_date_select;
    private TextView video_later_district_select;
    private TextView video_later_town_select;
    private ImageButton take_later_video;
    private ImageButton choose_later_video;
    private ImageButton send_media_button;
    private Uri video_uri;
    private static int VIDEO_REQUEST = 101;
    private MediaController mediaController;
    final int REQUEST_PERMISSION_CODE_CHOOSE_VIDEO_FILE = 1001;
    Uri videoFileUri;
    private ArrayList<District> alldistrict;

    Calendar calendar;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_later_declaration);
        init();
        action_onclick();
    }

    public void init(){
        con = new Connection(this);
        video_later_date_select = findViewById(R.id.video_later_select_date);
        video_later_district_select = findViewById(R.id.video_later_discrict_select);
        video_later_town_select = findViewById(R.id.video_later_town_select);
        take_later_video = findViewById(R.id.take_video_later_button);
        choose_later_video = findViewById(R.id.choose_video_later_button);
        send_media_button = findViewById(R.id.save_media_button);
        videoView = findViewById(R.id.video_later_videoview);
        mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
        claimtosave = new Claims();
    }

    public void action_onclick(){
        take_later_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPermissionFromDevice()){
                    requestPermission();
                }else{
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    video_uri = getPath();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, video_uri);
                    intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                    startActivityForResult(intent, VIDEO_REQUEST);
                }
            }
        });

        choose_later_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPermissionFromDevice()){
                    requestPermission();
                }else{
                    int readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                    if(checkPermissionFromDevice()) {
                        selectVideoFile();
                    }else {
                        requestPermission();
                    }
                }
            }
        });

        video_later_date_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                Toast.makeText(VideoLaterDeclarationActivity.this, ""+day+" "+ month +" "+year, Toast.LENGTH_SHORT).show();

                datePickerDialog = new DatePickerDialog(VideoLaterDeclarationActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int Myear, int Mmonth, int MdayOfMonth) {
                        video_later_date_select.setText(MdayOfMonth + "/"+ (Mmonth+1) + "/" + Myear);
                    }
                }, day, month, year);
                datePickerDialog.show();
            }
        });

        video_later_town_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(VideoLaterDeclarationActivity.this);
                mBuilder.setTitle("Liste des villes");
                CitiesDAO citiesDAO = new CitiesDAO(con.getDb());
                ArrayList<Cities> arraycities = new ArrayList<>();
                arraycities = citiesDAO.getAllCities();

                final String table[];
                table = new String[arraycities.size()];
                for(int i = 0; i < arraycities.size(); i++){
                    Cities c = new Cities();
                    c = arraycities.get(i);
                    table[i] = c.getCity_name();
                }
                final ArrayList<Cities> finalArraycities = arraycities;
                mBuilder.setSingleChoiceItems(table, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Cities c = finalArraycities.get(which);
                        video_later_town_select.setText(table[which]);
                        Toast.makeText(VideoLaterDeclarationActivity.this, ""+c.getCity_name(), Toast.LENGTH_SHORT).show();

                        alldistrict = new ArrayList<>();
                        DistrictDAO districtDAO = new DistrictDAO(con.getDb());
                        alldistrict = districtDAO.getAllDistrict(c.getId_city());
                    }
                });

                mBuilder.setNeutralButton("Choisir not enr", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        video_later_district_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alldistrict == null){
                    Toast.makeText(VideoLaterDeclarationActivity.this, "Veuillez selectionner une ville", Toast.LENGTH_SHORT).show();
                }else {

                    Toast.makeText(VideoLaterDeclarationActivity.this, "Click sur la selection du quartier", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(VideoLaterDeclarationActivity.this);
                    mBuilder.setTitle("Liste des villes");
                    final String table[];
                    table = new String[alldistrict.size()];
                    for (int i = 0; i < alldistrict.size(); i++) {
                        District c = new District();
                        c = alldistrict.get(i);
                        table[i] = c.getDistrict_name();
                    }
                    final ArrayList<District> finalArraycities = alldistrict;
                    mBuilder.setSingleChoiceItems(table, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            District c = finalArraycities.get(which);
                            video_later_district_select.setText(table[which]);
                            claimtosave.setPlace_district(c.getId_district());
                            Toast.makeText(VideoLaterDeclarationActivity.this, "" + claimtosave.getPlace_district(), Toast.LENGTH_SHORT).show();


                        }
                    });

                    mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog mDialog = mBuilder.create();
                    mDialog.show();
                }
            }
        });
        
        send_media_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(video_later_date_select.getText().toString() == "" || video_later_district_select.getText().toString() == "" || video_later_town_select.getText().toString() == "" || video_uri == null ){
                    Toast.makeText(VideoLaterDeclarationActivity.this, "Completer les champs", Toast.LENGTH_SHORT).show();
                }else {
                    final DAO<Claims> claimdao = new ClaimsDAO(con.getDb());
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    String currentDateTimeString = dateFormat.format(cal.getTime());
                    claimtosave.setVideo_content(video_uri.getPath());
                    claimtosave.setDeclaim(1); // Change it with the user id
                    claimtosave.setSaved_on(currentDateTimeString);
                    claimtosave.setClaim_status(2);
                    claimtosave.setDelete_state(0);
                    try {
                        ((ClaimsDAO) claimdao).add_claims(claimtosave);
                        Toast.makeText(VideoLaterDeclarationActivity.this, "ok", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(VideoLaterDeclarationActivity.this, "non", Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(VideoLaterDeclarationActivity.this, "Enregistrement réussi", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void selectVideoFile(){
        // Create an intent with ACTION_GET_CONTENT.
        Intent selectAudioIntent = new Intent(Intent.ACTION_GET_CONTENT);

        // Show audio in the content browser.
        // Set selectAudioIntent.setType("*/*") to select all data
        // Intent for this action must set content type, otherwise android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.GET_CONTENT } will be thrown
        selectAudioIntent.setType("video/*");

        // Start the activity.
        startActivityForResult(selectAudioIntent, REQUEST_PERMISSION_CODE_CHOOSE_VIDEO_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == VIDEO_REQUEST){
            if (resultCode == RESULT_OK){
                Toast.makeText(this, "ca marche", Toast.LENGTH_SHORT).show();
                videoView.setVideoURI(video_uri);
                videoView.setBackground(null);
                videoView.start();

            }
            else{
                Toast.makeText(this, "ca marche pas", Toast.LENGTH_SHORT).show();
            }
        }

        if(requestCode == REQUEST_PERMISSION_CODE_CHOOSE_VIDEO_FILE)
        {
            if(resultCode==RESULT_OK) { 
                videoFileUri = data.getData();
                videoView.setVideoURI(videoFileUri);
            }else{
                Toast.makeText(this, "Veuillez selectionner une autre video not enre", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){

            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    requestPermission();
                }
            }
            break;
        }
    }

    private boolean checkPermissionFromDevice() {
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int take_photo_result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                read_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                take_photo_result == PackageManager.PERMISSION_GRANTED;
    }

    public void saveImageToGallery(Bitmap bitmap){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root+"/nol");

    }

    public boolean isExternalStorageWritable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            Log.i("writeveri","is OK");
            return true;
        }
        else {
            return false;
        }
    }

    public Uri getPath(){
        File video_file = null;
        Uri apkURI = null;
        if(isExternalStorageWritable()) {
            File folder = new File(Environment.getExternalStorageDirectory() + "/ong_agir/video");

            if (!folder.exists()) {
                folder.mkdirs();
            }
            video_file = new File(folder, UUID.randomUUID().toString()+"claim_video.mp4");
            apkURI = FileProvider.getUriForFile(
                    this,
                    this.getApplicationContext()
                            .getPackageName() + ".provider", video_file);

        }
        return apkURI;
    }

}
