package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tg.agir.ong.agir_app.entities.Region;

public class RegionDAO extends DAO<Region> {
    //table name
    private static final String table_name = "region";

    //Table columns
    private static final String column_id_region = "id_region";
    private static final String column_region_name = "region_name";
    private static final String column_from_country = "from_country";
    private static final String column_delete_state = "delete_state";

    public RegionDAO(SQLiteDatabase database) {
        super(database);
    }

    public void add_country(Region country){
        ContentValues values = new ContentValues();
        values.put(column_id_region, country.getId_region());
        values.put(column_region_name, country.getRegion_name());
        values.put(column_from_country, country.getFrom_country());
        values.put(column_delete_state, 0);
        long inserResult = db.insert(table_name,null,values);
    }

    public Region select_specific_region(int id){

        Region region = new Region();

        String query =  "SELECT * FROM " + table_name + " WHERE " + column_delete_state + " = " + id;
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {

                region.setId_region(cursor.getInt(0));
                region.setRegion_name(cursor.getString(2));
                region.setDelete_state(cursor.getInt(7));
            }
            while (cursor.moveToNext());
        }
        db.close();

        return region;
    }
}
