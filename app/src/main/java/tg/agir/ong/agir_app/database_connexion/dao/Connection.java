package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Serializable;

public class Connection extends SQLiteOpenHelper {
    private final static int database_version = 3;
    private final static String database_name = "ong_agir.db";
    private SQLiteDatabase db;
    private static final String users_table = "CREATE TABLE IF NOT EXISTS users (id_user INTEGER PRIMARY KEY AUTOINCREMENT ," +
            " user_lastname TEXT, user_firsname TEXT, user_youngname TEXT, user_pseudo TEXT, user_photo TEXT, user_gender TEXT," +
            " user_birthdate TEXT, user_nationality TEXT, user_phone_number TEXT, sexual_tendencies TEXT, marital_status TEXT, " +
            "is_employed INTEGER, profession TEXT, user_email TEXT, user_password TEXT, serological_status TEXT, chat_password " +
            "TEXT, user_account_confirmation TEXT, user_password_recover_link TEXT, saved_on TEXT)";
    private static final String claims_table = "CREATE TABLE IF NOT EXISTS claims (id_claim INTEGER PRIMARY KEY AUTOINCREMENT, declaim INTEGER, date_claim_occur TEXT, " +
            "audio_content TEXT, video_content TEXT, text_content TEXT, place_longitude TEXT, place_latitude TEXT, saved_on TEXT, claim_status INTEGER, delete_state INTEGER)";
    private static final String folder_Administrator_table = "CREATE TABLE IF NOT EXISTS users (id_folder_administrator, INTEGER PRIMARY KEY AUTOINCREMENT , id_folder Integer, id_expert Integer, saved_by Integer, saved_on TEXT, modified_by INTEGER, modified_on INTEGER, delete_state INTEGER)";
    private static final String chat_table = "CREATE TABLE IF NOT EXISTS chat (id_chat INTEGER PRIMARY KEY AUTOINCREMENT , id_sender INTEGER, type_sender INTEGER, id_receiver INTEGER, type_receiver INTEGER, content_text TEXT, content_audio TEXT, content_video TEXT, saved_on TEXT, saved_by INTEGER, modified_on TEXT, modified_by INTEGER, delete_state INTEGER)";
    private static final String expert_table = "CREATE TABLE IF NOT EXISTS experts (id_expert INTEGER PRIMARY KEY AUTOINCREMENT , expert_lastname TEXT, expert_firstname TEXT, expert_photo TEXT, expert_email TEXT, expert_type Integer, expert_contact_1 TEXT, expert_contact_2 TEXT, delete_state TEXT)";
    private static final String countries_table = "CREATE TABLE IF NOT EXISTS countries (id_country INTEGER PRIMARY KEY AUTOINCREMENT , country_name TEXT, country_phone_code TEXT, delete_state INTEGER)";
    private static final String region_table = "CREATE TABLE IF NOT EXISTS region (id_region INTEGER PRIMARY KEY AUTOINCREMENT , region_name TEXT, from_country INTEGRER, delete_state INTEGER)";
    private static final String cities_table = "CREATE TABLE IF NOT EXISTS cities (id_city INTEGER PRIMARY KEY AUTOINCREMENT , city_name TEXT,from_region INTEGER, delete_state INTEGER)";
    private static final String district_table = "CREATE TABLE IF NOT EXISTS district (id_district INTEGER PRIMARY KEY AUTOINCREMENT , district_name TEXT, from_city INTEGER, delete_state INTEGER)";

    public static final String users_table_drop = "DROP TABLE IF EXISTS users;";
    public static final String claims_table_drop = "DROP TABLE IF EXISTS claims;";

    public Connection(Context context) {
        super(context, database_name, null, database_version);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(users_table);
        db.execSQL(folder_Administrator_table);
        db.execSQL(expert_table);
        db.execSQL(chat_table);
        db.execSQL(claims_table);
        db.execSQL(countries_table);
        db.execSQL(region_table);
        db.execSQL(cities_table);
        db.execSQL(district_table);
        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(users_table_drop);
        db.execSQL(claims_table_drop);
        onCreate(db);
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void close() {
        db.close();
    }


}
