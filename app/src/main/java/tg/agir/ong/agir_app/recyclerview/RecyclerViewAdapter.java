package tg.agir.ong.agir_app.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.RecyclerView;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.chat.ChatActivity;
import tg.agir.ong.agir_app.chat.Message;
import tg.agir.ong.agir_app.util.Util;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

    private List<ItemObject> itemList;
    private Context context;
    private final View.OnClickListener mOnClickListener = new MyOnClickListener();
    private Intent chatIntent;
    private RecyclerView mRecyclerView;


    public RecyclerViewAdapter(Context context, List<ItemObject> itemList) {
        this.itemList = itemList;
        this.context = context;
        chatIntent = new Intent(context, ChatActivity.class);

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.declaration_fragment, null);

        mRecyclerView = view.findViewById(R.id.recycler_claims_view);

        layoutView.setOnClickListener(mOnClickListener);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        if(itemList.get(position)==null){
            return;
        }

        Pattern p = Pattern.compile( "du (.*)" );
        Matcher m = p.matcher( itemList.get(position).getName() );
        String s= itemList.get(position).getName() ;
        String title_without_date=itemList.get(position).getName() ;
        if ( m.find() ) {
             s = m.group(1); // " that is awesome"

        }

            title_without_date = itemList.get(position).getName() .replaceAll("du (.*)", "");

        if(itemList.get(position).getTextContent()!=null)
        holder.content.setText(itemList.get(position).getTextContent());

        if(itemList.get(position)!=null&&itemList.get(position).getMessageList().size()>0){
            int index_of_last_message= itemList.get(position).getMessageList().size();
            String text_to_show= " Audio envoyé";
            if(!"null".equals(itemList.get(position).getMessageList().get(index_of_last_message-1).text)){
                text_to_show=  itemList.get(position).getMessageList().get(index_of_last_message-1).text;

            }
            holder.content.setText(text_to_show);
        }
        holder.avatarImage.setImageResource(itemList.get(position).getPhotoId());
        try {

            Date date = new SimpleDateFormat(Util.determineDateFormat(s), Locale.FRANCE).parse(s);
            String jour = new SimpleDateFormat("d MMMM yyyy").format(date);

            holder.title.setText(title_without_date +" du " +jour);

            String time = new SimpleDateFormat("HH:mm").format(date);
            holder.time.setText("" +time);

        } catch (ParseException e) {
            //Handle exception here
            e.printStackTrace();
        }

        ;
        if (itemList.get(position).getCount_new_message() != 0) {
            holder.countNewMessage.setVisibility(View.VISIBLE);
            holder.countNewMessage.setText("" + itemList.get(position).getCount_new_message());
        } else {
            holder.countNewMessage.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            if (mRecyclerView != null) {

                int itemPosition = mRecyclerView.getChildLayoutPosition(view);
                ItemObject item =Util.loadSharedPreferencesObjectList(context).get(itemPosition);
                itemList.get(itemPosition).setCount_new_message(0);
                item.setCount_new_message(0);

                List<Message> messageList = new ArrayList<>();
                Message sender = new Message(item.getName());
                sender.setId_claim(item.getId_claim());
                if (item.getVideo_path().isEmpty() && item.getAudio_path().isEmpty()) {
                    sender.setText(item.getText());
                }
                sender.setIs_sender(true);

                Message received = new Message(item.getTextContent());
                received.setIs_sender(false);
                received.setId_claim(item.getId_claim());

                messageList.add(sender);
                messageList.add(received);
                if (item.getMessageList().size() < 2) {
                    chatIntent.putExtra("list", (Serializable) messageList);

                } else {
                    chatIntent.putExtra("list", (Serializable) item.getMessageList());
                }
                Util.saveSharedPreferencesObjectList(view.getContext(),itemList);
                itemList = Util.loadSharedPreferencesObjectList(context);
                itemList.clear();
                notifyDataSetChanged();
                itemList.addAll(Util.loadSharedPreferencesObjectList(view.getContext()));
                view.getContext().startActivity(chatIntent);

            }
        }
    }

}
