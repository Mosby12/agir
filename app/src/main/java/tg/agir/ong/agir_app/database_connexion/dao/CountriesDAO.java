package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import tg.agir.ong.agir_app.entities.Countries;
import tg.agir.ong.agir_app.entities.Users;

public class CountriesDAO extends DAO<Countries>{
    //Table name
    private static final String table_name = "countries";

    //Table columns
    private static final String column_id_country = "id_country";
    private static final String column_country_name = "country_name";
    private static final String column_country_phone_code = "country_phone_code";
    private static final String column_delete_state = "delete_state";

    public CountriesDAO(SQLiteDatabase database) {
        super(database);
    }

    public void add_country(Countries country){
        ContentValues values = new ContentValues();
        values.put(column_id_country, country.getId_country());
        values.put(column_country_name, country.getCountry_name());
        values.put(column_country_phone_code, country.getCountry_phone_code());
        values.put(column_delete_state, 0);
        long inserResult = db.insert(table_name,null,values);
    }

    public Countries select_specific_country(int id){

        Countries country = new Countries();

        String query =  "SELECT * FROM " + table_name + " WHERE " + column_delete_state + " = " + id;
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {

                country.setId_country(cursor.getInt(0));
                country.setCountry_name(cursor.getString(2));
                country.setDelete_state(cursor.getInt(8));
            }
            while (cursor.moveToNext());
        }
        db.close();

        return country;
    }
}
