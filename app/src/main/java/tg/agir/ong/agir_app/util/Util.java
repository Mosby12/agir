package tg.agir.ong.agir_app.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.content.ContextCompat;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.recyclerview.ItemObject;


public class Util {
    static Gson gson = new Gson();
    static List<ItemObject> allItems = new ArrayList<ItemObject>();
    static ItemObject sample_ItemObject;


     static final Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {{
        put("^\\d{8}$", "yyyyMMdd");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
        put("^\\d{12}$", "yyyyMMddHHmm");
        put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
        put("^\\d{14}$", "yyyyMMddHHmmss");
        put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
    }};

    public static boolean hasPermission(Activity activity, String permission) {

        return (ContextCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED);
    }

    public static List<ItemObject> getAllItemList() {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
      /*
        for (int i = 0; i < 1; i++) {
            allItems.add(new ItemObject("Declaration du " + dateFormat.format(currentTime), "Donneees de Test", R.drawable.medical_report, currentTime));
        }
*/

        return allItems;
    }

    public static String loadSampledData() {
        return gson.toJson(allItems);
    }

    public static Gson getGson() {
        return Util.gson;
    }

    public static List<ItemObject> getAllItems() {
        return allItems;
    }

    public static ItemObject getSample_ItemObject() {
        return sample_ItemObject;
    }

    public static void setSample_ItemObject(ItemObject sample_ItemObject) {
        Util.sample_ItemObject = sample_ItemObject;
    }

    public static void saveSharedPreferencesObjectList(Context context, List<ItemObject> items) {
        SharedPreferences mPrefs = context.getSharedPreferences("App1", context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(items);
        prefsEditor.putString("appData_", json);
        prefsEditor.commit();
        allItems=items;
    }

    public static List<ItemObject> loadSharedPreferencesObjectList(Context context) {
        List<ItemObject> items = new ArrayList<ItemObject>();
        SharedPreferences mPrefs = context.getSharedPreferences("App1", context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("appData_", "");
        if (json.isEmpty()) {
            items = new ArrayList<ItemObject>();
        } else {
            Type type = new TypeToken<List<ItemObject>>() {
            }.getType();
            items = gson.fromJson(json, type);
        }

        return items;
    }

    public static  String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    public static String getNameFromUri(Context context, Uri uri) {
        String fileName="";
        if (uri.getScheme().equals("file")) {
            fileName = uri.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return fileName;
    }
    public static void getClaimsFromDB(Context context) {
        final Context main_context=context;
        final List<ItemObject> itemObjects = new ArrayList<ItemObject>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        String phoneNumber = preferences.getString("globalPhoneNumber", "");
        AsyncTask<String, Void, Void> asyncTask = new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... params) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://www.ongagir.com/claims.php?phone=" + params[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONObject object_ = new JSONObject(response.body().string());
                    JSONArray array  = object_.getJSONArray("claims");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);

                        ItemObject itemObject = new ItemObject();
                        itemObject.setId_claim(object.getString("id_claim"));
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(object.getString("saved_on"));
                        itemObject.setDate(date);
                        itemObject.setName(object.getString("text_content"));
                        if(!object.getString("video_content").isEmpty()){
                            itemObject.setVideo_path(object.getString("video_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.video_camera);
                        }
                        if(!object.getString("audio_content").isEmpty()){
                            itemObject.setAudio_path(object.getString("audio_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.volume);

                        }
                        if(object.getString("audio_content").isEmpty()&&object.getString("video_content").isEmpty()){
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.medical_report);
                        }
                        itemObjects.add(itemObject);

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                //adapter.notifyDataSetChanged();
                saveSharedPreferencesObjectList(main_context,itemObjects);
            }
        };

        asyncTask.execute(phoneNumber);
    }

    // Checkers -----------------------------------------------------------------------------------

    /**
     * Determine SimpleDateFormat pattern matching with the given date string. Returns null if
     * format is unknown. You can simply extend DateUtil with more formats if needed.
     * @param dateString The date string to determine the SimpleDateFormat pattern for.
     * @return The matching SimpleDateFormat pattern, or null if format is unknown.
     * @see SimpleDateFormat
     */
    public static String determineDateFormat(String dateString) {
        for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
            if (dateString.toLowerCase().matches(regexp)) {
                return DATE_FORMAT_REGEXPS.get(regexp);
            }
        }
        return null; // Unknown format.
    }
}