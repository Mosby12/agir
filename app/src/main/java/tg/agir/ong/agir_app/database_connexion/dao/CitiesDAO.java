package tg.agir.ong.agir_app.database_connexion.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import tg.agir.ong.agir_app.entities.Cities;

public class CitiesDAO extends DAO<Cities> {
    //table name
    private static final String table_name = "cities";

    //table columns
    private static final String column_city_id = "id_city";
    private static final String column_city_name = "city_name";
    private static final String column_from_region = "from_region";
    private static final String column_delete_state = "delete_state";

    private Cities us;

    public CitiesDAO(SQLiteDatabase pcon) {
        super(pcon);
    }

    //Methode for add cities
    public void add_cities(Cities cities){
        ContentValues values = new ContentValues();
        values.put(column_city_name,cities.getCity_name());
        values.put(column_from_region,cities.getFrom_region());
        values.put(column_delete_state, cities.getDelete_state());
        db.insert(table_name, null ,values);
    }

    public ArrayList getAllCities(){

        Log.i("getAllUsers", "getAllUsers");
        String query =  "SELECT * FROM "+ table_name + " WHERE " + column_delete_state + " = 0";
        ArrayList<Cities> citestList = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do {
                Cities city = new Cities();
                city.setId_city(cursor.getString(0));
                city.setCity_name(cursor.getString(1));
                citestList.add(city);
            }
            while (cursor.moveToNext());
        }


        Log.i("getAllUsersEnd", "getAllUsersEnd");
        return citestList;
    }
}
