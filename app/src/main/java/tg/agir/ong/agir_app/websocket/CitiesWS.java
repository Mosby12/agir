package tg.agir.ong.agir_app.websocket;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import tg.agir.ong.agir_app.WebSocketClientManager;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;

public class CitiesWS  {
    // The web socket instance variable
    private WebSocketClientManager currentWebSocketClient;

    // Content the current state of the connection with the server
    private boolean isConnectedToServer;

    // The current Activity
    public Activity currentActivity;
    // Content the current state of the database connection
    public Connection currentDBConnection;

    public CitiesWS(Activity activity, Connection dbConnection) {

        this.currentActivity = activity;
        this.currentDBConnection = dbConnection;

    }

    public boolean OnGetCitiesMessage(Map<String, String> dataMap) {
        Log.i("customISntersMessage", "ent");

        final String dataLength = dataMap.get("length");

        Log.i("customISntersMessage1", "ent");

        final String cities = dataMap.get("cities_list");

        Log.i("customIcities", "" + cities);
        Log.i("customIdataLength", "" + dataLength);

        // Reading list of articles in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
        //String articlesList = preferences.getString("globalarticlesList", "");
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("globalCitiesList", cities);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        editor.putString("globalCitiesListLastUpdateDate", currentDateandTime);
        editor.commit();

        Type mapType = new TypeToken<Map<String, Map>>() {
        }.getType();
        //final Map<String, String> articlesDataMap = new Gson().fromJson(articles,Map.class);

        return true;
    }
}
