package tg.agir.ong.agir_app;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;

import tg.agir.ong.agir_app.fragment.FragmentCenters;
import tg.agir.ong.agir_app.fragment.FragmentInstantDeclaration;
import tg.agir.ong.agir_app.fragment.FragmentSensitization;

public class DeclarationActivity extends FragmentActivity {
    private AppBarLayout appBarLayout;
    private TabLayout tablayout1;
    private ViewPager viewPager1;

    private FragmentInstantDeclaration frament_declaration;
    private FragmentCenters fragment_centrals;
    private FragmentSensitization frangment_sensitization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        frament_declaration = new FragmentInstantDeclaration();
        fragment_centrals = new FragmentCenters();
        frangment_sensitization = new FragmentSensitization();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declaration);
        appBarLayout =  findViewById(R.id.applicationbar_id);
        tablayout1 =  findViewById(R.id.declaration_menu_tablayout_id);
        viewPager1 =  findViewById(R.id.declaration_menu_viewpager_id);
        ViewPagerAdapter adapter1 = new ViewPagerAdapter(getSupportFragmentManager());

        //adapter1.addFragment(frament_declaration,"DECLARATION");
        adapter1.addFragment(frangment_sensitization,"NOUVELLES");
        adapter1.addFragment(fragment_centrals,"CENTRES");

        viewPager1.setAdapter(adapter1);
        tablayout1.setupWithViewPager(viewPager1);

    }
}
