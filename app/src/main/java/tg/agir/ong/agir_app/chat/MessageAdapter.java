package tg.agir.ong.agir_app.chat;

import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;
import tg.agir.ong.agir_app.R;

/**
 * Created by Varun John on 4 Dec, 2018
 * Github : https://github.com/varunjohn
 */
public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Handler.Callback {

    private List<Message> messages = new ArrayList<>();
    private static SimpleDateFormat timeFormatter = new SimpleDateFormat("m:ss", Locale.getDefault());
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private static final int VIEW_TYPE_AUDIO = 3;

    private static final int MSG_UPDATE_SEEK_BAR = 1845;

    private MediaPlayer mediaPlayer;

    private Handler uiUpdateHandler;

    private int playingPosition;
    private AudioItemsViewHolder playingHolder;

    public void add(Message message) {
        messages.add(message);
        notifyItemInserted(messages.lastIndexOf(message));
        uiUpdateHandler = new Handler(this);

    }
/*
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_message, null);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MessageViewHolder) holder).bind(messages.get(position));
    }*/

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static String getAudioTime(long time) {
        time *= 1000;
        timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return timeFormatter.format(new Date(time));
    }

    @Override
    public int getItemViewType(int position) {
        Message message = (Message) messages.get(position);
        if (!message.getAudio_path().isEmpty() && message.type == Message.TYPE_AUDIO) {
            return VIEW_TYPE_AUDIO;
        } else {
            if (message.isSender()) {
                // If the current user is the sender of the message
                return VIEW_TYPE_MESSAGE_SENT;
            } else {
                // If some other user sent the message
                return VIEW_TYPE_MESSAGE_RECEIVED;
            }
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_AUDIO) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cell, parent, false);
            return new AudioItemsViewHolder(view);
        }
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_message, parent, false);
            return new MessageViewHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_received_message, parent, false);
            return new ReceivedMessageViewHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = (Message) messages.get(position);
        String AudioSavePathInDevice =
                Environment.getExternalStorageDirectory().getPath() + "/" + message.getAudio_path();
        File file = new File(AudioSavePathInDevice);
        Log.e("MA", "FIlE NOT FOUND : " + AudioSavePathInDevice);

      if (message.type == Message.TYPE_AUDIO && !message.getAudio_path().isEmpty() && !file.exists()) {
            message.text = "Le fichier n'est plus présent sur le peripherique de stockage";
            message.type = Message.TYPE_TEXT;
        }

        if (!message.getAudio_path().isEmpty() && message.type == Message.TYPE_AUDIO) {
            int recordTime = getAudioLength(message.getAudio_path()) / 1000;

            message.time = recordTime;
            ((AudioItemsViewHolder) holder).bind(message);
        } else {
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_MESSAGE_SENT:
                    ((MessageViewHolder) holder).bind(message);
                    break;
                case VIEW_TYPE_MESSAGE_RECEIVED:
                    ((ReceivedMessageViewHolder) holder).bind(message);
            }
        }
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView text;
        public TextView in_coming;


        public MessageViewHolder(View view) {
            super(view);
            text = itemView.findViewById(R.id.textView);
        }

        public void bind(final Message message) {
            if (message.type == Message.TYPE_TEXT) {
                text.setText(message.text);

            } else {
                text.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                text.setText("");
            }
        }
    }

    public class ReceivedMessageViewHolder extends RecyclerView.ViewHolder {

        public TextView text;
        public TextView in_coming;


        public ReceivedMessageViewHolder(View view) {
            super(view);
            in_coming = itemView.findViewById(R.id.incoming);
        }

        public void bind(final Message message) {
            if (message.type == Message.TYPE_AUDIO) {
                in_coming.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mic_small, 0, 0, 0);
                in_coming.setText(String.valueOf(getAudioTime(message.time)));

            } else if (message.type == Message.TYPE_TEXT) {
                in_coming.setText(message.text);

            } else {
                in_coming.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                in_coming.setText("");
            }
        }
    }

    // Interaction listeners e.g. click, seekBarChange etc are handled in the view holder itself. This eliminates
    // need for anonymous allocations.
    public class AudioItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
        SeekBar sbProgress;
        ImageView ivPlayPause;
        TextView tvIndex;
        public TextView text;

        AudioItemsViewHolder(View itemView) {
            super(itemView);
            ivPlayPause = (ImageView) itemView.findViewById(R.id.ivPlayPause);
            ivPlayPause.setOnClickListener(this);
            sbProgress = (SeekBar) itemView.findViewById(R.id.sbProgress);
            sbProgress.setOnSeekBarChangeListener(this);
            tvIndex = (TextView) itemView.findViewById(R.id.tvIndex);
            text = itemView.findViewById(R.id.textView);

        }

        public void bind(final Message message) {
            tvIndex.setText(String.valueOf(getAudioTime(message.time)));

        }

        @Override
        public void onClick(View v) {

            if (mediaPlayer != null && getAdapterPosition() == playingPosition) {
                // toggle between play/pause of audio

                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                } else {
                    Log.e("MA", "FIlE NOT FOUND : " + getAdapterPosition());

                    mediaPlayer.start();
                }
            } else {
                // start another audio playback
                playingPosition = getAdapterPosition();
                if (mediaPlayer != null) {
                    if (null != playingHolder) {
                        updateNonPlayingView(playingHolder);
                    }
                    mediaPlayer.release();
                }
                playingHolder = this;
                if (messages.get(playingPosition) != null) {
                    startMediaPlayer(messages.get(playingPosition).getAudio_path());
                }
                //startMediaPlayer(messages.get(playingPosition).audioResId);
            }
            updatePlayingView();
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                mediaPlayer.seekTo(progress);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    private void startMediaPlayer(String path) {
        String AudioSavePathInDevice =
                Environment.getExternalStorageDirectory().getPath() + "/" + path;
        File file = new File(AudioSavePathInDevice);
        Log.e("MA", "FIlE NOT FOUND : " + AudioSavePathInDevice);

        if (!file.exists()) {
            return;

        }
        Uri uri = Uri.parse(AudioSavePathInDevice);
        mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(AudioSavePathInDevice);
            mediaPlayer.prepare();
        } catch (IllegalStateException e) {
            Log.d("MA", "IllegalStateException: " + e.getMessage());
        } catch (IOException e) {
            Log.d("MA", "IOException: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            Log.d("MA", "IllegalArgumentException: " + e.getMessage());
        } catch (SecurityException e) {
            Log.d("MA", "SecurityException: " + e.getMessage());
        }
        //mediaPlayer = MediaPlayer.create(getApplicationContext(), audioResId);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                releaseMediaPlayer();
            }
        });
        mediaPlayer.start();
    }

    private void releaseMediaPlayer() {
        if (null != playingHolder) {
            updateNonPlayingView(playingHolder);
        }
        mediaPlayer.release();
        mediaPlayer = null;
        playingPosition = -1;
    }

    /**
     * Changes the view to non playing state
     * - icon is changed to play arrow
     * - seek bar disabled
     * - remove seek bar updater, if needed
     *
     * @param holder ViewHolder whose state is to be chagned to non playing
     */
    private void updateNonPlayingView(AudioItemsViewHolder holder) {
        if (holder == playingHolder) {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
        }
        holder.sbProgress.setEnabled(false);
        holder.sbProgress.setProgress(0);
        holder.ivPlayPause.setImageResource(R.drawable.ic_play_arrow);
    }

    /**
     * Changes the view to playing state
     * - icon is changed to pause
     * - seek bar enabled
     * - start seek bar updater, if needed
     */
    private void updatePlayingView() {
        playingHolder.sbProgress.setMax(mediaPlayer.getDuration());
        playingHolder.sbProgress.setProgress(mediaPlayer.getCurrentPosition());
        playingHolder.sbProgress.setEnabled(true);
        if (mediaPlayer.isPlaying()) {
            uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
            playingHolder.ivPlayPause.setImageResource(R.drawable.ic_pause);
        } else {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
            playingHolder.ivPlayPause.setImageResource(R.drawable.ic_play_arrow);
        }
    }

    void stopPlayer() {
        if (null != mediaPlayer) {
            releaseMediaPlayer();
        }
    }

    public boolean handleMessage(android.os.Message msg) {
        switch (msg.what) {
            case MSG_UPDATE_SEEK_BAR: {
                playingHolder.sbProgress.setProgress(mediaPlayer.getCurrentPosition());
                uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                return true;
            }
        }
        return false;
    }

    public int getAudioLength(String pathStr) {
        String path = Environment.getExternalStorageDirectory().getPath() + "/" + pathStr;
        String mediaPath = Uri.parse(path).getPath();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mediaPath);
        String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mmr.release();
        return Integer.parseInt(duration);
    }
}