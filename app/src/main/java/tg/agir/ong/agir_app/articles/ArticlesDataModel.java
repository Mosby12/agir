package tg.agir.ong.agir_app.articles;

import java.util.Map;

import tg.agir.ong.agir_app.R;

public class ArticlesDataModel {
    private int imageDrawable;
    private String title;
    private String content;
    private String url;
    private String bannerUrl;
    private String lang;

    public ArticlesDataModel(Map<String, String> data) {
        imageDrawable = R.drawable.article_default_image;
        title = data.get("article_title").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
        content = data.get("article_content").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
        url = data.get("article_url").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
        bannerUrl = data.get("banner_img").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
        lang = data.get("publishing_language").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\"");
        //title = String.format(Locale.ENGLISH, "Title %d Goes Here", id);
        //content = String.format(Locale.ENGLISH, "Sub title %d goes here", id);
    }

    public int getImageDrawable() {
        return imageDrawable;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getUrl() {
        return url;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public String getLang() {
        return lang;
    }
}