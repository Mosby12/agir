package tg.agir.ong.agir_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import tg.agir.ong.agir_app.articles.ArticlesDataModel;
import tg.agir.ong.agir_app.database_connexion.dao.CitiesDAO;
import tg.agir.ong.agir_app.database_connexion.dao.ClaimsDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.database_connexion.dao.DistrictDAO;
import tg.agir.ong.agir_app.entities.Cities;
import tg.agir.ong.agir_app.entities.Claims;
import tg.agir.ong.agir_app.entities.District;
import tg.agir.ong.agir_app.websocket.CitiesWS;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.Inflater;

public class AudioLaterDeclarationActivity extends AppCompatActivity {
    private Connection con;
    private Claims claimtosave;
    private List<Claims> claimsList;

    private ListView listView;
    private ImageButton send_audio_declaration;
    private ImageButton restart;
    private ImageButton recordingstart;
    private ImageButton select_recording_audio;
    private AppCompatButton count_bt;
    private TextView select_date;
    private TextView select_district;
    private TextView select_town;
    MediaRecorder mediaRecorder;
    String path = "";
    MediaPlayer mediaPlayer;
    private Intent intent;
    final int REQUEST_PERMISSION_CODE = 1000;
    final int REQUEST_PERMISSION_CODE_CHOOSE_AUDIO_FILE = 1001;
    private Runnable runnable;
    private Handler handler;
    private ListAdapter adapter;
    private ArrayList<District> alldistrict;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    private Uri audioFileUri = null;
    //String citiesList = "";
    private WebSocketClientManager webSocketClientManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_later_declaration);

        init();
        action_onclick();
    }

    public void init(){
        con = new Connection(this);
        claimtosave = new Claims();
        send_audio_declaration = findViewById(R.id.send_audio_later_declaration);
        restart = findViewById(R.id.restart_audio_later_declaration);
        recordingstart = findViewById(R.id.recording_later_audio_start);
        select_recording_audio = findViewById(R.id.select_recording_audio);
        count_bt = findViewById(R.id.count_later_bt);
        select_date = findViewById(R.id.audio_later_select_date);
        select_town = findViewById(R.id.audio_later_town_select);
        select_district = findViewById(R.id.audio_later_discrict_select);
        handler = new Handler();
        listView = findViewById(R.id.audio_later_listview);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String citiesList = preferences.getString("globalCitiesList", "");
        Log.i("citiesList", "" + citiesList);

        if (!citiesList.equals("")) {
            String formattedCitiesList = citiesList.replace("_FIRST_POINT_", ":");
            final Map<String, String> citiesDataMap = new Gson().fromJson(formattedCitiesList, Map.class);

            for (int i = 0; i < citiesDataMap.size(); i++) {
                String subFormattedCitiesList = citiesDataMap.get("" + i).replace("_SECOND_POINT_", "\":\"");
                subFormattedCitiesList = subFormattedCitiesList.replace("_OPEN_ACCOLADE_", "{\"");
                subFormattedCitiesList = subFormattedCitiesList.replace("_COMMA_", "\",\"");
                subFormattedCitiesList = subFormattedCitiesList.replace("_CLOSE_ACCOLADE_", "\"}");
                Log.i("Cities", "" + subFormattedCitiesList);
                final Map<String, String> citiesSubDataMap = new Gson().fromJson(subFormattedCitiesList, Map.class);

                Cities c = new Cities();
                c.setId_city(citiesSubDataMap.get("id_city").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\""));
                c.setCity_name(citiesSubDataMap.get("city_name").replace("_REAL_POINT_", ":").replace("_REAL_DOUBLE_QUOTE_", "\""));
                // Il te faudra inserer les districts  récupérés ici

                CitiesDAO citiesDAO = new CitiesDAO(con.getDb());
                citiesDAO.add_cities(c);

            }
        }

        /**CitiesDAO citiesDAO1 = new CitiesDAO(con.getDb());
        DistrictDAO districtDAO1 = new DistrictDAO(con.getDb());
        Cities c1 = new Cities();c1.setCity_name("Lomé");c1.setDelete_state(0);
        Cities c2 = new Cities();c2.setCity_name("Kpalimé");c2.setDelete_state(0);
        District d1 = new District();d1.setDistrict_name("Kpota");d1.setDelete_state(0);d1.setFrom_city(2);
        District d2 = new District();d2.setDistrict_name("Be");d2.setDelete_state(0);d2.setFrom_city(1);

        citiesDAO1.add_cities(c1); citiesDAO1.add_cities(c2);
        districtDAO1.add_district(d1); districtDAO1.add_district(d2); **/
    }

    public void action_onclick(){

        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();

                datePickerDialog = new DatePickerDialog(AudioLaterDeclarationActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int Myear, int Mmonth, int MdayOfMonth) {
                        select_date.setText(MdayOfMonth + "/"+ (Mmonth+1) + "/" + Myear);
                    }
                }, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
                datePickerDialog.show();
            }
        });

        select_town.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(AudioLaterDeclarationActivity.this);
                mBuilder.setTitle("Liste des villes");
                CitiesDAO citiesDAO = new CitiesDAO(con.getDb());
                ArrayList<Cities> arraycities = new ArrayList<>();
                arraycities = citiesDAO.getAllCities();

                final String table[];
                table = new String[arraycities.size()];
                for(int i = 0; i < arraycities.size(); i++){
                    Cities c = new Cities();
                    c = arraycities.get(i);
                    table[i] = c.getCity_name();
                }
                final ArrayList<Cities> finalArraycities = arraycities;
                mBuilder.setSingleChoiceItems(table, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Cities c = finalArraycities.get(which);
                        select_town.setText(table[which]);
                        Toast.makeText(AudioLaterDeclarationActivity.this, ""+c.getCity_name(), Toast.LENGTH_SHORT).show();

                        alldistrict = new ArrayList<>();
                        DistrictDAO districtDAO = new DistrictDAO(con.getDb());
                        alldistrict = districtDAO.getAllDistrict(c.getId_city());
                    }
                });

                mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        select_district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alldistrict == null){
                    Toast.makeText(AudioLaterDeclarationActivity.this, "Veuillez selectionner une ville", Toast.LENGTH_SHORT).show();
                }else {

                    Toast.makeText(AudioLaterDeclarationActivity.this, "Click sur la selection du quartier", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(AudioLaterDeclarationActivity.this);
                    mBuilder.setTitle("Liste des quartier");
                    final String table[];
                    table = new String[alldistrict.size()];
                    for (int i = 0; i < alldistrict.size(); i++) {
                        District c = new District();
                        c = alldistrict.get(i);
                        table[i] = c.getDistrict_name();
                    }
                    final ArrayList<District> finalArraydistricts = alldistrict;
                    mBuilder.setSingleChoiceItems(table, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            District c = finalArraydistricts.get(which);
                            select_district.setText(table[which]);
                            claimtosave.setPlace_district(c.getId_district());
                            Toast.makeText(AudioLaterDeclarationActivity.this, "" + claimtosave.getPlace_district(), Toast.LENGTH_SHORT).show();
                        }
                    });

                    mBuilder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog mDialog = mBuilder.create();
                    mDialog.show();
                }
            }
        });

        select_recording_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AudioLaterDeclarationActivity.this, "choix", Toast.LENGTH_SHORT).show();
                int readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                if(checkPermissionFromDevice()) {
                    selectAudioFile();
                }else {
                    requestPermission();
                }
            }
        });

            recordingstart.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if(event.getAction() == MotionEvent.ACTION_DOWN){

                            if(checkPermissionFromDevice()){
                                Toast.makeText(AudioLaterDeclarationActivity.this, "En ", Toast.LENGTH_SHORT).show();
                                setupMediaRecorder();
                                try {
                                    mediaRecorder.prepare();
                                    mediaRecorder.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }else{
                                requestPermission();
                            }

                        }else if(event.getAction() == MotionEvent.ACTION_UP) {
                            mediaRecorder.stop();
                            claimsList = new ArrayList<>();
                            Claims claim = new Claims();
                            claimsList.add(claim);
                            adapter = new ListAdapter();
                            listView.setAdapter(adapter);
                            Toast.makeText(AudioLaterDeclarationActivity.this, "Fin ", Toast.LENGTH_SHORT).show();
                        }

                    }catch (Exception e){

                    }

                    return false;
                }
            });

            send_audio_declaration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(select_date.getText().toString() == "" || select_district.getText().toString() == "" || select_town.getText().toString() == "" ||path == ""){
                        Toast.makeText(AudioLaterDeclarationActivity.this, "vjiide", Toast.LENGTH_SHORT).show();
                    }else{
                        if(checkPermissionFromDevice()){
                            try {

                                if(mediaPlayer != null){
                                    mediaPlayer.stop();
                                    mediaPlayer.release();
                                    setupMediaRecorder();
                                }

                                final DAO<Claims> claimdao = new ClaimsDAO(con.getDb());
                                Calendar cal = Calendar.getInstance();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                String currentDateTimeString = dateFormat.format(cal.getTime());
                                claimtosave.setSaved_on(currentDateTimeString);
                                claimtosave.setAudio_content(path);
                                claimtosave.setVideo_content("");
                                claimtosave.setText_content("");
                                claimtosave.setDate_claim_occur(select_date.getText()+"");
                                claimtosave.setClaim_status(2);
                                claimtosave.setDelete_state(0);
                                ((ClaimsDAO) claimdao).add_claims(claimtosave);
                                Toast.makeText(AudioLaterDeclarationActivity.this, "Arrêt d'enregistremnbent", Toast.LENGTH_SHORT).show();
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else{
                            requestPermission();
                        }
                    }
                }
            });

            restart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
    }

    public void selectAudioFile(){
        // Create an intent with ACTION_GET_CONTENT.
        Intent selectAudioIntent = new Intent(Intent.ACTION_GET_CONTENT);

        // Show audio in the content browser.
        // Set selectAudioIntent.setType("*/*") to select all data
        // Intent for this action must set content type, otherwise android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.GET_CONTENT } will be thrown
        selectAudioIntent.setType("audio/*");

        // Start the activity.
        startActivityForResult(selectAudioIntent, REQUEST_PERMISSION_CODE_CHOOSE_AUDIO_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_PERMISSION_CODE_CHOOSE_AUDIO_FILE)
        {
            if(resultCode==RESULT_OK)
            {
                // To make example simple and clear, we only choose audio file from
                // local file, this is easy to get audio file real local path.
                // If you want to get audio file real local path from a audio content provider
                // Please read another article.
                audioFileUri = data.getData();

                path = RealPathUtil.getRealPath(this, audioFileUri);

                Toast.makeText(this, ""+ path, Toast.LENGTH_SHORT).show();
                //Toast.makeText(this, ""+ Environment.getExternalStorageDirectory().getAbsolutePath()+audioFileUri.getPath(), Toast.LENGTH_SHORT).show();

                //path = Environment.getExternalStorageDirectory().getAbsolutePath()+audioFileUri.getPath();
                claimsList = new ArrayList<>();
                Claims claim = new Claims();
                claimsList.add(claim);
                adapter = new ListAdapter();
                listView.setAdapter(adapter);
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        }, REQUEST_PERMISSION_CODE);
    }

    private void setupMediaRecorder(){
        path = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/ong_agir/audio/"
                + UUID.randomUUID().toString()+"_audio_record.3gp";
        Toast.makeText(this, ""+path, Toast.LENGTH_SHORT).show();
        mediaRecorder = new MediaRecorder();
        Log.i("record", mediaRecorder.toString());
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(path);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){

            case REQUEST_PERMISSION_CODE:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private boolean checkPermissionFromDevice() {
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int recording_audio_result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                read_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                recording_audio_result == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isExternalStorageWritable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            Log.i("writeveri","is OK");
            return true;
        }
        else {
            return false;
        }
    }

    public class ListAdapter extends BaseAdapter {
        private View view;
        private SeekBar duration = null;
        private MediaPlayer mediaPlayer;
        private ImageButton play;
        private Runnable runnable;
        private Handler handler;
        private Boolean isPlaying = false;

        @Override
        public int getCount() {
            return claimsList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.audio_custom_view, null);
            duration = (SeekBar) view.findViewById(R.id.audio_custom_seekbar);
            play = (ImageButton) view.findViewById(R.id.audio_custom_play);
            
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isPlaying == false) {
                        stopCurrentPlayAudio();
                        initAudioPlayer();
                        play.setImageResource(R.drawable.ic_pause_black_24dp);
                        mediaPlayer.start();
                        isPlaying = true;
                        Toast.makeText(AudioLaterDeclarationActivity.this, "Test playing audio", Toast.LENGTH_SHORT).show();
                    }else{
                        play.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        Log.i("Audiochange",play.toString());
                        mediaPlayer.pause();
                        isPlaying = false;

                    }

                }
            });

            return view;
        }

        public void initAudioPlayer() {
            try {
                if(mediaPlayer == null)
                {
                    mediaPlayer = new MediaPlayer();

                    if(path == "")
                    {
                        Toast.makeText(AudioLaterDeclarationActivity.this, "Veuillez Faire un enregistrement", Toast.LENGTH_SHORT).show();
                    }else {
                            mediaPlayer.setDataSource(path);
                            mediaPlayer.prepare();


                    }
                }
            }catch(IOException ex)
            {
                Toast.makeText(AudioLaterDeclarationActivity.this, ""+ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        public void stopCurrentPlayAudio() {
            if(mediaPlayer!=null && mediaPlayer.isPlaying())
            {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }

    }

}
