package tg.agir.ong.agir_app.entities;

public class Users {
    private int id_user;
    private String user_lastname;
    private String user_firstname;
    private String user_youngname;
    private String user_pseudo;
    private String user_photo;
    private String user_gender;
    private String user_birthdate;
    private String user_nationality;
    private String user_phone_number;
    private String sexual_tendencies;
    private String marital_status;
    private int is_employed;
    private String profession;
    private String user_email;
    private String user_password;
    private String user_encrypt_code;
    private String serological_status;
    private String chat_password;
    private String user_account_confirmation;
    private String user_password_recover_link;
    private String user_device_configuration;
    private String saved_on;

    public Users() {
        this.user_lastname = "";
        this.user_firstname = "";
        this.user_youngname = "";
        this.user_pseudo = "";
        this.user_photo = "";
        this.user_gender = "";
        this.user_birthdate = "";
        this.user_nationality = "";
        this.user_phone_number = "";
        this.sexual_tendencies = "";
        this.marital_status = "";
        this.is_employed = -1;
        this.profession = "";
        this.user_email = "";
        this.user_password = "";
        this.user_encrypt_code = "";
        this.serological_status = "";
        this.chat_password = "";
        this.user_account_confirmation = "";
        this.user_password_recover_link = "";
        this.user_device_configuration = "";
        this.saved_on = "";
    }

    /*public Users(String user_email, String user_password) {
        this.user_email = user_email;
        this.user_password = user_password;
    }
*/

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUser_lastname() {
        return user_lastname;
    }

    public void setUser_lastname(String user_lastname) {
        this.user_lastname = user_lastname;
    }

    public String getUser_firstname() {
        return user_firstname;
    }

    public void setUser_firstname(String user_firstname) {
        this.user_firstname = user_firstname;
    }

    public String getUser_youngname() {
        return user_youngname;
    }

    public void setUser_youngname(String user_youngname) {
        this.user_youngname = user_youngname;
    }

    public String getUser_pseudo() {
        return user_pseudo;
    }

    public void setUser_pseudo(String user_pseudo) {
        this.user_pseudo = user_pseudo;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_birthdate() {
        return user_birthdate;
    }

    public void setUser_birthdate(String user_birthdate) {
        this.user_birthdate = user_birthdate;
    }

    public String getUser_nationality() {
        return user_nationality;
    }

    public void setUser_nationality(String user_nationality) {
        this.user_nationality = user_nationality;
    }

    public String getUser_phone_number() {
        return user_phone_number;
    }

    public void setUser_phone_number(String user_phone_number) {
        this.user_phone_number = user_phone_number;
    }

    public String getSexual_tendencies() {
        return sexual_tendencies;
    }

    public void setSexual_tendencies(String sexual_tendencies) {
        this.sexual_tendencies = sexual_tendencies;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public int getIs_employed() {
        return is_employed;
    }

    public void setIs_employed(int is_employed) {
        this.is_employed = is_employed;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_encrypt_code() {
        return user_encrypt_code;
    }

    public void setUser_encrypt_code(String user_encrypt_code) {
        this.user_encrypt_code = user_encrypt_code;
    }

    public String getSerological_status() {
        return serological_status;
    }

    public void setSerological_status(String serological_status) {
        this.serological_status = serological_status;
    }

    public String getChat_password() {
        return chat_password;
    }

    public void setChat_password(String chat_password) {
        this.chat_password = chat_password;
    }

    public String getUser_account_confirmation() {
        return user_account_confirmation;
    }

    public void setUser_account_confirmation(String user_account_confirmation) {
        this.user_account_confirmation = user_account_confirmation;
    }

    public String getUser_password_recover_link() {
        return user_password_recover_link;
    }

    public void setUser_password_recover_link(String user_password_recover_link) {
        this.user_password_recover_link = user_password_recover_link;
    }

    public String getUser_device_configuration() {
        return user_device_configuration;
    }

    public void setUser_device_configuration(String user_device_configuration) {
        this.user_device_configuration = user_device_configuration;
    }

    public String getSaved_on() {
        return saved_on;
    }

    public void setSaved_on(String saved_on) {
        this.saved_on = saved_on;
    }
}
