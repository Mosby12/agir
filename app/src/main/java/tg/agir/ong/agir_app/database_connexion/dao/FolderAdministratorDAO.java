package tg.agir.ong.agir_app.database_connexion.dao;

import android.database.sqlite.SQLiteDatabase;

import tg.agir.ong.agir_app.entities.FolderAdministrator;

public class FolderAdministratorDAO extends  DAO<FolderAdministrator> {

    private static final String table_name = "folder_administrator";
    private FolderAdministrator fa;

    //Table users columns

    private static final String column_id_folder_administrator="id_folder_administrator";
    private static final String column_id_folder="id_folder";
    private static final String column_id_expert ="id_expert";
    private static final String column_saved_by="saved_by";
    private static final String column_saved_on="saved_on";
    private static final String column_modified_by="modified_by";
    private static final String column_modified_on="modified_on";
    private static final String column_delete_state="delete_state";


    public FolderAdministratorDAO(SQLiteDatabase db) {

        super(db);
    }


    public FolderAdministrator search_Folder(SQLiteDatabase database){
        return fa;
    }

}
