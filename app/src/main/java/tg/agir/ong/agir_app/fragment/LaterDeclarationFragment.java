package tg.agir.ong.agir_app.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tg.agir.ong.agir_app.AudioLaterDeclarationActivity;
import tg.agir.ong.agir_app.HexagonMaskView;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.VideoLaterDeclarationActivity;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.WriteLaterDeclarationActivity;


public class LaterDeclarationFragment extends Fragment {
    private HexagonMaskView fragment_later_write_declaration_imageview;
    private HexagonMaskView fragment_later_video_declaration_imageview;
    private HexagonMaskView fragment_later_audio_declaration_imageview;
    private HexagonMaskView fragment_later_return_declaration_fragment;

    View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_later_declaration, container, false);
        init();
        actions_on_view();
        return view;
    }

    public void init(){
        fragment_later_write_declaration_imageview = view.findViewById(R.id.later_write_declaration_imageview_bt);
        fragment_later_video_declaration_imageview = view.findViewById(R.id.later_video_declaration_imageview_bt);
        fragment_later_audio_declaration_imageview = view.findViewById(R.id.later_audio_declaration_imageview_bt);
        fragment_later_return_declaration_fragment = view.findViewById(R.id.later_return_declaration_imageview_bt);
    }

    public void actions_on_view(){
        fragment_later_write_declaration_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), WriteLaterDeclarationActivity.class);
                startActivity(intent);
            }
        });

        fragment_later_video_declaration_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), VideoLaterDeclarationActivity.class);
                startActivity(intent);
            }
        });

        fragment_later_audio_declaration_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), AudioLaterDeclarationActivity.class);
                startActivity(intent);
            }
        });

        fragment_later_return_declaration_fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), WelcomeActivity.class);
                startActivity(intent);
            }
        });
    }

}
