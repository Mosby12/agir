package tg.agir.ong.agir_app.entities;

public class Sensitization {
    private int id_sensitization;
    private String target_gender;
    private String target_age;
    private String target_marital_status;
    private String target_sexual_tendencies;
    private String banner_img;
    private String content_text;
    private String saved_by;
    private String saved_on;
    private int modified_by;
    private String modified_on;
    private String starting_date;
    private String ending_date;
    private int delete_state;

    public Sensitization() {
    }

    public int getId_sensitization() {
        return id_sensitization;
    }

    public void setId_sensitization(int id_sensitization) {
        this.id_sensitization = id_sensitization;
    }

    public String getTarget_gender() {
        return target_gender;
    }

    public void setTarget_gender(String target_gender) {
        this.target_gender = target_gender;
    }

    public String getTarget_age() {
        return target_age;
    }

    public void setTarget_age(String target_age) {
        this.target_age = target_age;
    }

    public String getTarget_marital_status() {
        return target_marital_status;
    }

    public void setTarget_marital_status(String target_marital_status) {
        this.target_marital_status = target_marital_status;
    }

    public String getTarget_sexual_tendencies() {
        return target_sexual_tendencies;
    }

    public void setTarget_sexual_tendencies(String target_sexual_tendencies) {
        this.target_sexual_tendencies = target_sexual_tendencies;
    }

    public String getBanner_img() {
        return banner_img;
    }

    public void setBanner_img(String banner_img) {
        this.banner_img = banner_img;
    }

    public String getContent_text() {
        return content_text;
    }

    public void setContent_text(String content_text) {
        this.content_text = content_text;
    }

    public String getSaved_by() {
        return saved_by;
    }

    public void setSaved_by(String saved_by) {
        this.saved_by = saved_by;
    }

    public String getSaved_on() {
        return saved_on;
    }

    public void setSaved_on(String saved_on) {
        this.saved_on = saved_on;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getStarting_date() {
        return starting_date;
    }

    public void setStarting_date(String starting_date) {
        this.starting_date = starting_date;
    }

    public String getEnding_date() {
        return ending_date;
    }

    public void setEnding_date(String ending_date) {
        this.ending_date = ending_date;
    }

    public int getDelete_state() {
        return delete_state;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }
}
