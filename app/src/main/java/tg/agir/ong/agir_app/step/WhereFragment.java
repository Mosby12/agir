package tg.agir.ong.agir_app.step;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.recyclerview.ItemObject;
import tg.agir.ong.agir_app.util.Util;

import static android.content.Context.LOCATION_SERVICE;

public class WhereFragment extends Fragment implements BlockingStep {
    private EditText location_text;
    private Button location_Button;
    private Geocoder geocoder;
    private List<Address> addresses;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_step_where, container, false);
        location_text = (EditText) v.findViewById(R.id.location);
        location_Button = (Button) v.findViewById(R.id.Location_button);
        location_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showGPSDisabledAlertToUser();
                    return;
                }
                if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    showGPSDisabledAlertToUser();
                    return;
                }
                Toast.makeText(getActivity(), "Nous recuperons votre adresse ", Toast.LENGTH_SHORT).show();
                //showGPSDisabledAlertToUser();
                getLocation();
            }
        });

        /* initialize your UI */
        return v;
    }


    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public void onCompleteClicked(final StepperLayout.OnCompleteClickedCallback callback) {

        if (location_text != null & !location_text.getText().toString().isEmpty()) {
            Date currentTime = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH_mm_ss");
            String text = dateFormat.format(currentTime).replace('/', '_');
            ItemObject itemObject = new ItemObject();
            itemObject.setName("Declaration du " + text);
            itemObject.setText(Util.getSample_ItemObject().getText() + " à " + location_text.getText().toString());
            //itemObject.setLocation(location_text.getText().toString());
            itemObject.setPhotoId(R.drawable.medical_report);
            itemObject.setDate(currentTime);
            Util.setSample_ItemObject(itemObject);
            //you can do anythings you want
            callback.complete();
        } else {
            Toast.makeText(getActivity(), "Il est nécéssaire que vous nous indiquez une adresse ", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
    }

    public void onLocationChanged_(Location location) {


        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

//        Log.e("latitude", "latitude--" + latitude);
        try {
            //    Log.e("latitude", "inside latitude--" + latitude);
            if (geocoder != null)
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //  ro_gps_location.setText(state + " , " + city + " , " + country);
                location_text.setText(address);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (!Util.hasPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            return;
        }
        if (!Util.hasPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            return;
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            return;
        }
        geocoder = new Geocoder(this.getContext(), Locale.getDefault());

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(android.location.Location location) {
                onLocationChanged_(location);
                //  showGPSDisabledAlertToUser();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        });
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(android.location.Location location) {
                onLocationChanged_(location);
                //showGPSDisabledAlertToUser();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                getLocation();

            }
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getContext());
        alertDialogBuilder.setMessage("Le GPS est désactivé sur votre appareil. Souhaitez-vous l'activer ??")
                .setCancelable(false)
                .setPositiveButton("Activer la localisation",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}