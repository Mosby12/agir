package tg.agir.ong.agir_app.entities;

public class Region {
    private int id_region;
    private String region_name;
    private int from_country;
    private int delete_state;

    public Region() {

    }

    public int getId_region() {
        return id_region;
    }

    public void setId_region(int id_region) {
        this.id_region = id_region;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public int getFrom_country() {
        return from_country;
    }

    public void setFrom_country(int from_country) {
        this.from_country = from_country;
    }

    public int getDelete_state() {
        return delete_state;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }
}
