package tg.agir.ong.agir_app.entities;

public class Folder {
    private int id_folder;
    private int id_claim;
    private String folder_number;
    private int folder_status;

    public int getId_folder() {
        return id_folder;
    }

    public void setId_folder(int id_folder) {
        this.id_folder = id_folder;
    }

    public int getId_claim() {
        return id_claim;
    }

    public void setId_claim(int id_claim) {
        this.id_claim = id_claim;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public void setFolder_number(String folder_number) {
        this.folder_number = folder_number;
    }

    public int getFolder_status() {
        return folder_status;
    }

    public void setFolder_status(int folder_status) {
        this.folder_status = folder_status;
    }
}
