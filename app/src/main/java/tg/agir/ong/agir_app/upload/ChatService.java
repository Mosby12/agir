package tg.agir.ong.agir_app.upload;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tg.agir.ong.agir_app.MainActivity;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.chat.Message;
import tg.agir.ong.agir_app.recyclerview.ItemObject;
import tg.agir.ong.agir_app.util.Util;

public class ChatService extends Service {

    String tag = "ChatService";
    WeakReference<Context> context;
    Handler mHandler = new Handler();
    private Timer timer;
    private TimerTask timerTask;
    long oldTime = 0;

    public ChatService() {
    }

    public ChatService(Context context) {
        this.context = new WeakReference<Context>(context);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Toast.makeText(this, "Service created...", Toast.LENGTH_LONG).show();
        Log.i(tag, "Service created...");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        super.onStartCommand(intent, flags, startId);
        this.context = new WeakReference<Context>(this.getBaseContext());
        //  notificationDialog();
        getChatsFromDB(this.context.get());
        Log.i(tag, "Service started...");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //.makeText(this, "Service destroyed...", Toast.LENGTH_LONG).show();
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent(this, ChatBroadCastReceiver.class);
        sendBroadcast(broadcastIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    public void onTaskRemoved(Intent rootIntent) {

        //unregister listeners
        //do any other cleanup if required

        //stop service
        stopSelf();
    }

    private void refreshData() {
        Intent declaration_intent = new Intent("fetch_data");
        LocalBroadcastManager.getInstance(this).sendBroadcast(declaration_intent);
        Intent chat_intent = new Intent("refresh_chat");
        LocalBroadcastManager.getInstance(this).sendBroadcast(chat_intent);
    }

    public void notificationDialog(List<Message> messageList) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "mofiala_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription("Message reçu");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        int id = 0;
        Intent notifyIntent = new Intent(this, MainActivity.class);

        // Set the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
// Create the PendingIntent
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                this, 4, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        for (Message message : messageList) {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            notificationBuilder.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.logo_agir)
                    .setTicker("MOFIALA")
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle("Message de Mofiala")
                    .setContentText("" + message.getText())
                    .setContentIntent(notifyPendingIntent)
                    .setContentInfo("Reponse à votre declaration")
                    .setGroup("Agir Group");
            notificationManager.notify(id++, notificationBuilder.build());
        }
    }

    public void getChatsFromDB(final Context context) {
        final Context main_context = context;
        final List<ItemObject> itemObjects = new ArrayList<ItemObject>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        String phoneNumber = preferences.getString("globalPhoneNumber", "");
        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                String result = "KO";

                OkHttpClient client = getUnsafeOkHttpClient();
                Request request = new Request.Builder()
                        .url("https://www.ongagir.com/chat.php?phone=" + params[0])
                        .build();
                try {

                    Response response = client.newCall(request).execute();
                    String incoming_data = response.body().string();
                    JSONObject object_ = new JSONObject(incoming_data);
                    JSONArray array = object_.getJSONArray("claims");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);
                        String user_id = object.getString("user_id");

                        ItemObject itemObject = new ItemObject();
                        itemObject.setId_claim(object.getString("id_claim"));
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = simpleDateFormat.parse(object.getString("saved_on"));
                        itemObject.setDate(date);
                        itemObject.setName(object.getString("text_content"));
                        if (!object.getString("video_content").isEmpty()) {
                            itemObject.setVideo_path(object.getString("video_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.video_camera);
                        }
                        if (!object.getString("audio_content").isEmpty()) {
                            itemObject.setAudio_path(object.getString("audio_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.volume);

                        }
                        if (object.getString("audio_content").isEmpty() && object.getString("video_content").isEmpty()) {
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setText(object.getString("text_content"));
                            itemObject.setName("Declaration ecrite du " + simpleDateFormat.format(itemObject.getDate()));
                            itemObject.setPhotoId(R.drawable.medical_report);
                        }
                        JSONArray lineItems = object.getJSONArray("messages");

                        List<Message> messages = new ArrayList<>();
                        for (int k = 0; k < lineItems.length(); k++) {
                            JSONObject jsonLineItem = lineItems.getJSONObject(k);

                            String content_text = jsonLineItem.getString("content_text");
                            String audio_path = jsonLineItem.getString("context_audio");

                            Message message = new Message(content_text);
                            message.setId_claim(itemObject.getId_claim());
                            if (audio_path != null && !audio_path.isEmpty()) {
                                message.type = Message.TYPE_AUDIO;
                                message.setAudio_path(audio_path);
                            }
                            if (!user_id.equals(jsonLineItem.getString("id_sender"))) {
                                message.setIs_sender(false);
                            }
                            messages.add(message);

                        }
                        itemObject.setMessageList(messages);
                        itemObjects.add(itemObject);

                    }
                    result = "OK";

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }catch (NullPointerException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                //adapter.notifyDataSetChanged();
                if (result.equals("OK")) {
                    if(Util.loadSharedPreferencesObjectList(main_context)==null) {
                        return;
                    }
                    //Util.saveSharedPreferencesObjectList(main_context, itemObjects);
                    List<Message> messageToNotify = new ArrayList<>();
                    int i = 0;
                    for (ItemObject new_item : itemObjects) {
                        ItemObject saved_item = Util.loadSharedPreferencesObjectList(main_context).get(i);
                        List<Message> received_messages = extract_all_received_message(new_item.getMessageList());
                        List<Message> local_messages = extract_all_received_message(saved_item.getMessageList());


                        if (received_messages.size() > local_messages.size()) {

                            int diff = received_messages.size() - local_messages.size();
                            Log.i(tag, "Diff  ... " + diff);

                            new_item.setCount_new_message(diff);
                                /*
                                Log.i(tag, "Diff  ... " + (local_messages.size() - 1)+" yyuy  "+(received_messages.size()-1));
                                if(local_messages.size() >0&& received_messages.size()>0)
                                messageToNotify.addAll(received_messages.subList(local_messages.size() , received_messages.size()));*/
                            received_messages.removeAll(local_messages);
                            messageToNotify.addAll(received_messages);
                        } else {
                            new_item.setCount_new_message(saved_item.getCount_new_message());
                        }

                        i++;
                    }

                    Util.saveSharedPreferencesObjectList(main_context, itemObjects);
                    refreshData();
                    if (!itemObjects.isEmpty()) {
                        if (!messageToNotify.isEmpty()) {
                            notificationDialog(messageToNotify);
                        } else {
                            Log.i(tag, "Objects ...");

                        }
                    }
                    // Toast.makeText(main_context, "Envoi reussi "+itemObjects.get(3).getMessageList().get(0).getText(), Toast.LENGTH_SHORT).show();
                } else {/*
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Erreur reseau !!");
                    builder.setMessage("Réseau indisponible!");

                    builder.setPositiveButton("Reesayer ?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });

                    try {
                        AlertDialog dialog = builder.create();

                        dialog.show();
                    } catch (Exception e) {

                    }*/

                }
            }
        };

        asyncTask.execute(phoneNumber);
    }

    public List<Message> extract_all_received_message(List<Message> messages) {
        List<Message> messageList = new ArrayList<>();
        for (Message msg : messages) {
            if (!msg.isSender()) {
                messageList.add(msg);
            }
        }

        return messageList;
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}