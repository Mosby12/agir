package tg.agir.ong.agir_app.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Marker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.articles.ArticlesAdapter;
import tg.agir.ong.agir_app.articles.ArticlesDataModel;

public class FragmentSensitization extends Fragment {
    View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public FragmentSensitization() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sensitization_fragment, container, false);

        mRecyclerView = view.findViewById(R.id.recycler_view);


        // Reading list of screening centers in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        String screeningCentersList = preferences.getString("globalArticlesList", "");

        if (!screeningCentersList.equals("")) {

            String formattedArticlesList = screeningCentersList.replace("_FIRST_POINT_", ":");
            Log.i("customIarticl5424", "" + formattedArticlesList);
            final Map<String, String> articlesDataMap = new Gson().fromJson(formattedArticlesList, Map.class);
            Log.i("customIarticllast", "enter");

            List<ArticlesDataModel> dataModelList = new ArrayList<>();
            TextView nothingToDisplayTextView = view.findViewById(R.id.text_view_label_nothing_to_display);
            nothingToDisplayTextView.setVisibility(View.INVISIBLE);
            
            for (int i = 0; i < articlesDataMap.size(); i++) {

                String subFormattedArticlesList = articlesDataMap.get("" + i).
                        replace("_SECOND_POINT_", "\":\"");
                subFormattedArticlesList = subFormattedArticlesList.replace("_OPEN_ACCOLADE_", "{\"");
                subFormattedArticlesList = subFormattedArticlesList.replace("_COMMA_", "\",\"");
                subFormattedArticlesList = subFormattedArticlesList.replace("_CLOSE_ACCOLADE_", "\"}");
                Log.i("customIarticlsecon", "" + subFormattedArticlesList);
                final Map<String, String> screeningCentersSubDataMap = new Gson().fromJson(subFormattedArticlesList, Map.class);

                dataModelList.add(new ArticlesDataModel(screeningCentersSubDataMap));

            }

        /*List<ArticlesDataModel> dataModelList = new ArrayList<>();
        for (int i = 1; i <= 20; ++i) {

            dataModelList.add(new ArticlesDataModel(i));
        }*/

        // use this setting to improve performance if you know that changes

        // in content do not change the layout size of the RecyclerView

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager

        mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter and pass in our data model list

        mAdapter = new ArticlesAdapter(dataModelList, view.getContext());
        mRecyclerView.setAdapter(mAdapter);
    }

        return view;
    }

    public void onPause(){
        super.onPause();
        Log.i("onPauseNews", ( " is on screen"));
    }

    @Override
    public void onResume() {
        super.onResume();
        WelcomeActivity.setActiveFragment(this.getClass().getSimpleName());
        Log.i("onResumeNews", ( " is on screen"));
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("onStopNews", ( " is on screen"));
    }
}
