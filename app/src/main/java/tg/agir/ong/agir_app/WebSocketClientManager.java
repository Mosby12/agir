/**
 * Class to manage web socket (connection, disconnection, message interception,...)
 */

package tg.agir.ong.agir_app;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hbb20.CountryCodePicker;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.database_connexion.dao.UsersDAO;
import tg.agir.ong.agir_app.entities.Users;
import tg.agir.ong.agir_app.websocket.ArticlesWS;
import tg.agir.ong.agir_app.websocket.CitiesWS;
import tg.agir.ong.agir_app.websocket.MapWS;
import tg.agir.ong.agir_app.websocket.SignInWS;

public class WebSocketClientManager extends Activity {

    // The web socket instance variable
    private WebSocketClient mWebSocketClient;

    // Content the current state of the connection with the server
    private boolean isConnectedToServer;

    // Content the current state of the connection with the server
    public Activity currentActivity;

    // Content the current state of the database connection
    public Connection currentDBConnection;

    public WebSocketClientManager(Activity activity, Connection dbConnection) {
        //connectWebSocket();
        this.isConnectedToServer = false;
        this.currentActivity = activity;
        this.currentDBConnection = dbConnection;
    }

    /**
     * Method to connect to websocket
     */
    public void connectWebSocket() {
        URI uri;
        try {
            String serverIP = currentActivity.getResources().getString(R.string.config_server_host_ip);
            String serverPort = currentActivity.getResources().getString(R.string.config_server_host_port);
            //Log.i("WebSocketCMserverIP", "ws://" + serverIP + ":" + serverPort + "/server.php");
            uri = new URI("ws://" + serverIP + ":" + serverPort + "/server.php");

            // Setting the uri of the server
            //uri = new URI("ws://192.168.43.34:9001/server.php");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        // Initialization of the web socket instance
        mWebSocketClient = new WebSocketClient(uri) {
            // If the connection to the server is open (successful)
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
                mWebSocketClient.send("Hello from mobile");
                isConnectedToServer = true;


                // Reading user phone number in preferences
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
                String phoneNumberValue = preferences.getString("globalPhoneNumber", "");
                Log.i("globalPhoneNumber", "" + phoneNumberValue);
                if (!phoneNumberValue.equals("")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Sending available screening centers fetching request to the server
                            Log.i("runWelc", "runWelc enter");
                            WebSocketClientManager.this.getScreeningCenters();
                        }
                    });
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Sending available articles fetching request to the server
                            Log.i("runWelc", "runWelc enter");
                            WebSocketClientManager.this.getArticles();
                        }
                    });
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Sending available articles fetching request to the server
                            Log.i("runWelc", "runWelc enter");
                            WebSocketClientManager.this.getCities();
                        }
                    });
                }
            }

            // If there is any message from the server
            @Override
            public void onMessage(String message) {
                //Log.i("onMessage", "enter");
                //final String message = s;
                final Map<String, String> dataMap = new Gson().fromJson(message, Map.class);
                final String receiverID = dataMap.get("send_to");

                // Reading user phone number in preferences
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
                String phoneNumberValue = preferences.getString("globalPhoneNumber", "");
                Log.i("globalPhoneNumberID", "" + "APP" + phoneNumberValue);

                if (phoneNumberValue.equals("")) {
                    // Country code picker biding from layout
                    CountryCodePicker countryCodePicker = (CountryCodePicker) currentActivity.findViewById(R.id.spinner_choose_country);
                    phoneNumberValue = countryCodePicker.getFullNumberWithPlus();
                    Log.i("onMessageEditText", receiverID + " = APP" + phoneNumberValue);
                }

                Log.i("receiverID", "" + "APP" + phoneNumberValue);
                if (receiverID.equals("APP" + phoneNumberValue)) {
                    Log.i("receiverIDEnt", "enter");
                    final String messageType = dataMap.get("type");

                    // When the message type is "appUserSignInServerReply"
                    if (messageType.equals("appUserSignInServerReply")) {
                        Log.i("messageType", "" + messageType);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SignInWS signInWS = new SignInWS(currentActivity, WebSocketClientManager.this.currentDBConnection);
                                signInWS.OnSignInAnswerMessage(dataMap);
                            }
                        });
                    }

                    // When the message type is "appGetScreeningCentersReply"
                    if (messageType.equals("appGetScreeningCentersReply")) {
                        Log.i("message", "" + message);
                        Log.i("messageTypelength", "" + dataMap.get("length"));

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("messageTyperun", "" + messageType);
                                MapWS mapWS = new MapWS(currentActivity, WebSocketClientManager.this.currentDBConnection);
                                Log.i("messageTyperun1", "" + messageType);
                                mapWS.OnGetScreeningCentersMessage(dataMap);
                                Log.i("messageTyperun2", "" + messageType);
                            }
                        });
                    }

                    // When the message type is "appGetArticlesReply"
                    if (messageType.equals("appGetArticlesReply")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("messageTyperun", "" + messageType);
                                ArticlesWS articlesWS = new ArticlesWS(currentActivity, WebSocketClientManager.this.currentDBConnection);
                                Log.i("messageTyperun1", "" + messageType);
                                articlesWS.OnGetArticlesMessage(dataMap);
                                Log.i("messageTyperun2", "" + messageType);
                            }
                        });
                    }

                    // When the message type is "appGetCitiesReply"
                    if (messageType.equals("appGetCitiesReply")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("messageTyperun", "" + messageType);
                                CitiesWS citiesWS = new CitiesWS(currentActivity, WebSocketClientManager.this.currentDBConnection);
                                Log.i("messageTyperun1", "" + messageType);
                                citiesWS.OnGetCitiesMessage(dataMap);
                                Log.i("messageTyperun2", "" + messageType);
                            }
                        });
                    }
                }


            }

            // If the connection to the server is close (failed or is lost)
            @Override
            public void onClose(int i, String s, boolean b) {
                isConnectedToServer = false;
                Log.i("Websocket", "Closed " + s);
            }

            // If there is any error during the connection to the server
            @Override
            public void onError(Exception e) {
                isConnectedToServer = false;
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        // Trying to connect to the server
        mWebSocketClient.connect();
    }

    public boolean isConnectedToServer() {
        return isConnectedToServer;
    }

    public boolean sendSignInMessage(Map messageToSend) {

        if (this.isConnectedToServer()) {
            messageToSend.put("\"type\"", "\"appUserSignIn\"");

            Log.i("sendSignInMessage", "" + messageToSend.toString());
            // Send sign in message to server
            this.mWebSocketClient.send("" + messageToSend.toString());
            return true;
        } else {
            return false;
        }
    }


    public boolean getScreeningCenters() {

        // Reading list of screening centers in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
        String screeningCentersList = preferences.getString("globalScreeningCentersList", "");
        String screeningCentersListLastUpdateDate = preferences.getString("globalScreeningCentersListLastUpdateDate", "");

        //Log.i("differenceBetweenDates", "" + differenceBetweenDates("2018-12-18 22:51:26", "2018-12-28 22:51:26", "D"));


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentDateandTime = sdf.format(new Date());


        if (!screeningCentersList.equals("") && !screeningCentersListLastUpdateDate.equals("")
                //&& differenceBetweenDates(currentDateandTime, screeningCentersListLastUpdateDate, "D") < 0  ){
                && differenceBetweenDates(currentDateandTime, screeningCentersListLastUpdateDate, "D") < 7  ){
            Log.i("customIEnt", "retrur false ");
            return false;
        }


        if (this.isConnectedToServer()) {
            Log.i("getScreeningCentersConn", "ok ");
            // Creating a map to contain the data to send to server
            Map<String, String> dataToSend = new HashMap<String, String>();

            // Put elements to the data
            dataToSend.put("\"type\"", "\"appGetScreeningCenters\"");
            // Reading user phone number in preferences
            //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
            String phoneNumberValue = preferences.getString("globalPhoneNumber", "");
            dataToSend.put("\"phoneNumber\"", "\""+phoneNumberValue+"\"");

            Log.i("runWelcdataToSend", "dataToSend " + dataToSend.toString());

            // Send a sign in request to the server
            this.mWebSocketClient.send(dataToSend.toString());
            return true;
        } else {
            return false;
        }

    }

    public boolean getArticles() {

        // Reading list of articles in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
        String articlesList = preferences.getString("globalArticlesList", "");
        String articlesListLastUpdateDate = preferences.getString("globalArticlesListLastUpdateDate", "");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentDateandTime = sdf.format(new Date());

        if (!articlesList.equals("") && !articlesListLastUpdateDate.equals("")
                //&& differenceBetweenDates(currentDateandTime, articlesListLastUpdateDate, "D") < 0  ){
            && differenceBetweenDates(currentDateandTime, articlesListLastUpdateDate, "D") < 7  ){
            Log.i("customIEnt", "retrur false ");
            return false;
        }


        if (this.isConnectedToServer()) {
            Log.i("customIEntConn", "ok ");
            // Creating a map to contain the data to send to server
            Map<String, String> dataToSend = new HashMap<String, String>();

            // Put elements to the data
            dataToSend.put("\"type\"", "\"appGetArticles\"");
            String phoneNumberValue = preferences.getString("globalPhoneNumber", "");
            dataToSend.put("\"phoneNumber\"", "\""+phoneNumberValue+"\"");

            Log.i("runWelcdataToSend", "dataToSend " + dataToSend.toString());

            // Send a sign in request to the server
            this.mWebSocketClient.send(dataToSend.toString());
            return true;
        } else {
            return false;
        }

    }

    public Long differenceBetweenDates(String firstDateString, String secondDateString, String timeUnit){
        if (firstDateString.equals("") || secondDateString.equals("")){
            return Long.parseLong("0");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            //convertedDate = dateFormat.parse(firstDateString);
            Date firstDate = sdf.parse(firstDateString);
            Date secondDate = sdf.parse(secondDateString);

            long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
            long diff = 0;
            if (timeUnit.equals("D")){
                diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            }
            if (timeUnit.equals("M")){
                diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
            }
            if (timeUnit.equals("S")){
                diff = TimeUnit.SECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            }
            Log.i("customI", firstDateString + " - " + secondDateString+  " = "+ diffInMillies + " => " + diff);
            return diff;
        } catch (ParseException e) {
            //e.printStackTrace();
            //Log.i("printStackTrace", "printStackTrace ");
            return Long.parseLong("0");
        }
    }

    public boolean getCities(){
        // Reading list of screening centers in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
        String citiesList = preferences.getString("globalCitiesList", "");
        String citiesListLastUpdateDate = preferences.getString("globalCitiesListLastUpdateDate", "");

        //Log.i("differenceBetweenDates", "" + differenceBetweenDates("2018-12-18 22:51:26", "2018-12-28 22:51:26", "D"));


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentDateandTime = sdf.format(new Date());


        if (!citiesList.equals("") && !citiesListLastUpdateDate.equals("")
                && differenceBetweenDates(currentDateandTime, citiesListLastUpdateDate, "D") < 1  ){
               // && differenceBetweenDates(currentDateandTime, citiesListLastUpdateDate, "D") < 7  ){
            Log.i("customIEnt", "retrur false ");
            return false;
        }


        if (this.isConnectedToServer()) {
            Log.i("getCitiesConn", "ok ");
            // Creating a map to contain the data to send to server
            Map<String, String> dataToSend = new HashMap<String, String>();

            // Put elements to the data
            dataToSend.put("\"type\"", "\"appGetCities\"");
            // Reading user phone number in preferences
            //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
            String phoneNumberValue = preferences.getString("globalPhoneNumber", "");
            dataToSend.put("\"phoneNumber\"", "\""+phoneNumberValue+"\"");

            Log.i("runWelcdataToSend", "dataToSend " + dataToSend.toString());

            // Send a sign in request to the server
            this.mWebSocketClient.send(dataToSend.toString());
            return true;
        } else {
            return false;
        }
    }

}
