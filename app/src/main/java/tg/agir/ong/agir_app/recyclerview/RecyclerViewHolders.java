package tg.agir.ong.agir_app.recyclerview;


import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import tg.agir.ong.agir_app.R;

public class RecyclerViewHolders extends RecyclerView.ViewHolder{

    public TextView title;
    public TextView content;
    public ImageView avatarImage;
    public TextView countNewMessage;
    public TextView time;


    public RecyclerViewHolders(View itemView) {
        super(itemView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            itemView.setOutlineProvider(ViewOutlineProvider.BACKGROUND);
        }

        final Context context = itemView.getContext();
        title = (TextView)itemView.findViewById(R.id.title);
        content = (TextView)itemView.findViewById(R.id.content);
        avatarImage = (ImageView)itemView.findViewById(R.id.avatarImage);
        countNewMessage = (TextView)itemView.findViewById(R.id.count);
        time = (TextView)itemView.findViewById(R.id.timestamp);
    }

}
