/**
 * Class to manage web socket sign in requests
 */

package tg.agir.ong.agir_app.websocket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;

import org.java_websocket.client.WebSocketClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.chat.Message;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.recyclerview.ItemObject;
import tg.agir.ong.agir_app.util.Util;

public class SignInWS extends Activity {

    private final CountryCodePicker countryCodePicker;
    private final EditText phoneNumberEditText;
    private final Button checkPhoneNumberButton;
    private final TextView incorrectPhoneNumberLabel;
    private final TextView codeConfirmationLabel;
    private final EditText codeConfirmationEditText;
    private final Button codeConfirmationButton;
    private final TextView incorrectCodeConfirmationLabel;
    private final TextView chooseCountryNumberLabel;
    private final TextView connectionLabel;
    // The web socket instance variable
    private WebSocketClient mWebSocketClient;

    // Content the current state of the connection with the server
    private boolean isConnectedToServer;
    // The current Activity
    public Activity currentActivity;
    // Content the current state of the database connection
    public Connection currentDBConnection;

    public SignInWS(Activity activity, Connection dbConnection) {

        this.currentActivity = activity;
        this.currentDBConnection = dbConnection;

        // Country code picker biding from layout
        countryCodePicker = (CountryCodePicker) activity.findViewById(R.id.spinner_choose_country);

        // Phone number biding from layout
        phoneNumberEditText = (EditText) activity.findViewById(R.id.edit_text_enter_phone);

        // Check Phone Number Button biding from layout
        checkPhoneNumberButton = (Button) activity.findViewById(R.id.button_check_phone_number);

        // Incorrect Phone Number Label TextView biding from layout
        incorrectPhoneNumberLabel = (TextView) activity.findViewById(R.id.text_view_label_error_enter_correct_phone_number);

        // Incorrect Phone Number Label TextView biding from layout
        connectionLabel = (TextView) activity.findViewById(R.id.text_view_label_connexion);

        // Incorrect Phone Number Label TextView biding from layout
        chooseCountryNumberLabel = (TextView) activity.findViewById(R.id.text_view_choose_country);

        // Code confirmation edit text biding from layout
        codeConfirmationEditText = (EditText) activity.findViewById(R.id.edit_text_enter_confirmation_code);

        // Code confirmation button biding from layout
        codeConfirmationButton = (Button) activity.findViewById(R.id.button_check_confirmation_code);

        // Code confirmation text view biding from layout
        codeConfirmationLabel = (TextView) activity.findViewById(R.id.text_view_label_confirm_code);

        // Code confirmation text view biding from layout
        incorrectCodeConfirmationLabel = (TextView) activity.findViewById(R.id.text_view_label_error_enter_correct_confirmation_code);

    }


    public boolean OnSignInAnswerMessage(Map<String, String> dataMap){

        final String confirmationCodeFromServer = dataMap.get("confirmation_code");
        final String phoneNumberFromServer = dataMap.get("phone_number");



        ViewGroup.LayoutParams params = phoneNumberEditText.getLayoutParams();
        params.height = 0;
        phoneNumberEditText.setLayoutParams(params);
        checkPhoneNumberButton.getLayoutParams().height = 0;
        incorrectPhoneNumberLabel.getLayoutParams().height = 0;
        connectionLabel.getLayoutParams().height = 0;
        chooseCountryNumberLabel.getLayoutParams().height = 0;

        countryCodePicker.setVisibility(View.INVISIBLE);
        phoneNumberEditText.setVisibility(View.INVISIBLE);
        checkPhoneNumberButton.setVisibility(View.INVISIBLE);
        incorrectPhoneNumberLabel.setVisibility(View.INVISIBLE);
        connectionLabel.setVisibility(View.INVISIBLE);
        chooseCountryNumberLabel.setVisibility(View.INVISIBLE);

        codeConfirmationEditText.setVisibility(View.VISIBLE);
        codeConfirmationButton.setVisibility(View.VISIBLE);
        codeConfirmationLabel.setVisibility(View.VISIBLE);


        // Adding click event listener to the check phone number button
        codeConfirmationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("clicknfirmationLabe", "enter");
                    // Getting the value of the input code
                    String confirmationCodeValue = codeConfirmationEditText.getText().toString().trim();


                Log.i("confirmationCodeValue", confirmationCodeValue + " = " + confirmationCodeFromServer);
                    // Checking if the country code and phone number exist
                    if (confirmationCodeValue != null && confirmationCodeValue.equals(confirmationCodeFromServer)) {
                        Log.i("verificonfirmationalue", "enter");
                        // Hide the error label
                        incorrectCodeConfirmationLabel.setVisibility(View.INVISIBLE);

                        // Save user phone number in preferences
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("globalPhoneNumber", phoneNumberFromServer);
                        editor.commit();
                        //String phoneNumber = preferences.getString("phoneNumber","");

                        // Setting the current view to WelcomeActivity
                        getClaimsFromDB(currentActivity);



                    } else {
                        incorrectCodeConfirmationLabel.setVisibility(View.VISIBLE);
                    }
                }
        });

        //JSONArray array=new JSONArray("["+obj.toString()+"]");
        return true;
    }
    public  void getClaimsFromDB(Context context) {
        final Context main_context=context;
        final List<ItemObject> itemObjects = new ArrayList<ItemObject>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        String phoneNumber = preferences.getString("globalPhoneNumber", "");
        AsyncTask<String, Void, Void> asyncTask = new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... params) {
                String result = "KO";
                trustAllHosts();

                OkHttpClient client = getUnsafeOkHttpClient();
                Request request = new Request.Builder()
                        .url("https://www.ongagir.com/chat.php?phone=" + params[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONObject object_ = new JSONObject(response.body().string());
                    JSONArray array = object_.getJSONArray("claims");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);
                        String user_id = object.getString("user_id");

                        ItemObject itemObject = new ItemObject();
                        itemObject.setId_claim(object.getString("id_claim"));
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = simpleDateFormat.parse(object.getString("saved_on"));
                        itemObject.setDate(date);
                        itemObject.setName(object.getString("text_content"));
                        if (!object.getString("video_content").isEmpty()) {
                            itemObject.setVideo_path(object.getString("video_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.video_camera);
                        }
                        if (!object.getString("audio_content").isEmpty()) {
                            itemObject.setAudio_path(object.getString("audio_content"));
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setPhotoId(R.drawable.volume);

                        }
                        if (object.getString("audio_content").isEmpty() && object.getString("video_content").isEmpty()) {
                            itemObject.setTextContent("Votre declaration est en cours de traitement");
                            itemObject.setText(object.getString("text_content"));
                            itemObject.setName("Declaration ecrite du " + simpleDateFormat.format(itemObject.getDate()));
                            itemObject.setPhotoId(R.drawable.medical_report);
                        }
                        JSONArray lineItems = object.getJSONArray("messages");

                        List<Message> messages = new ArrayList<>();
                        for (int k = 0; k < lineItems.length(); k++) {
                            JSONObject jsonLineItem = lineItems.getJSONObject(k);

                            String content_text = jsonLineItem.getString("content_text");
                            String audio_path = jsonLineItem.getString("context_audio");

                            Message message = new Message(content_text);
                            message.setId_claim(itemObject.getId_claim());
                            if (audio_path != null && !audio_path.isEmpty()) {
                                message.type = Message.TYPE_AUDIO;
                                message.setAudio_path(audio_path);
                            }
                            if (!user_id.equals(jsonLineItem.getString("id_sender"))) {
                                message.setIs_sender(false);
                            }
                            messages.add(message);

                        }
                        itemObject.setMessageList(messages);
                        itemObjects.add(itemObject);

                    }
                    result = "OK";

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                //adapter.notifyDataSetChanged();

                Util.saveSharedPreferencesObjectList(main_context,itemObjects);
                Intent intent = new Intent(currentActivity, WelcomeActivity.class);
                currentActivity.startActivity(intent);
                currentActivity.finish();
            }
        };

        asyncTask.execute(phoneNumber);
    }
    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void trustAllHosts() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
        }
    }



}
