package tg.agir.ong.agir_app.adapter;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.entities.Experts;

public class ListExpertsRecyclerViewAdapter extends RecyclerView.Adapter<ListExpertsRecyclerViewAdapter.MyViewHolder> {



 private   List<Experts> listExpert;
   private  String title_expert;

    public ListExpertsRecyclerViewAdapter(List<Experts> listExpert) {
        this.listExpert = listExpert;
    }

    @Override
    public int getItemCount() {
        return this.listExpert.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // CREATE VIEW HOLDER AND INFLATING ITS XML LAYOUT
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.model_view_discut, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
     holder.display(this.listExpert.get(position));
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name = ((TextView) itemView.findViewById(R.id.text_nom));
        private final TextView Title= ((TextView) itemView.findViewById(R.id.text_titre));
        private final ImageView Image_profile = ((ImageView) itemView.findViewById(R.id.Image_profile));

        public MyViewHolder(final View itemView) {
            super(itemView);




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  new AlertDialog.Builder(itemView.getContext())
                    //      .setTitle(currentPair.first)
                    //     .setMessage(currentPair.second)
                    //   .show();
                }
            });
        }

        public void display(Experts listExpert) {




        if(listExpert.getExpert_type()==1){
            title_expert="";
        }
        else if (listExpert.getExpert_type()==2){
            title_expert="";

        }else if (listExpert.getExpert_type()==3){
            title_expert="";

            }
               Title.setText(title_expert);
            name.setText(listExpert.getExpert_lastname()+""+listExpert.getExpert_firstname());


    }
}}

