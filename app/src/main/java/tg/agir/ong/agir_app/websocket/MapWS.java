/**
 * Class to manage web socket sign in requests
 */

package tg.agir.ong.agir_app.websocket;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hbb20.CountryCodePicker;

import org.java_websocket.client.WebSocketClient;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Marker;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import tg.agir.ong.agir_app.R;
import tg.agir.ong.agir_app.WebSocketClientManager;
import tg.agir.ong.agir_app.WelcomeActivity;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;

public class MapWS extends Activity {

    // The web socket instance variable
    private WebSocketClientManager currentWebSocketClient;

    // Content the current state of the connection with the server
    private boolean isConnectedToServer;

    // The current Activity
    public Activity currentActivity;
    // Content the current state of the database connection
    public Connection currentDBConnection;

    public MapWS(Activity activity, Connection dbConnection) {

        this.currentActivity = activity;
        this.currentDBConnection = dbConnection;

    }


    public boolean OnGetScreeningCentersMessage(Map<String, String> dataMap) {
        Log.i("OnGetSntersMessage", "ent");

        final String dataLength = dataMap.get("length");
        Log.i("OnGetSntersMessage1", "ent");

        final String screeningCenters = dataMap.get("screening_centers_list");
        //final String screeningCenters = dataMap.get("screening_centers_list").replace("=",":");

        Log.i("screeningCenters", "" + screeningCenters);
        Log.i("dataLength", "" + dataLength);

        // Reading list of screening centers in preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
        //String screeningCentersList = preferences.getString("globalScreeningCentersList", "");
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("globalScreeningCentersList", screeningCenters);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        editor.putString("globalScreeningCentersListLastUpdateDate", currentDateandTime);
        editor.commit();

        Type mapType = new TypeToken<Map<String, Map>>() {
        }.getType();
        //final Map<String, String> screeningCentersDataMap = new Gson().fromJson(screeningCenters,Map.class);

        /*
        String formattedScreeningCentersList = screeningCenters.replace("_FIRST_POINT_", ":");
        Log.i("customI5424", "" + formattedScreeningCentersList);
        final Map<String, String> screeningCentersDataMap = new Gson().fromJson(formattedScreeningCentersList, Map.class);
        Log.i("customIlast", "enter");

        for (int i = 0; i < screeningCentersDataMap.size(); i++) {

            String subFormattedScreeningCentersList = screeningCentersDataMap.get("" + i).
                    replace("_SECOND_POINT_", "\":\"");
            subFormattedScreeningCentersList = subFormattedScreeningCentersList.replace("_OPEN_ACCOLADE_", "{\"");
            subFormattedScreeningCentersList = subFormattedScreeningCentersList.replace("_COMMA_", "\",\"");
            subFormattedScreeningCentersList = subFormattedScreeningCentersList.replace("_CLOSE_ACCOLADE_", "\"}");
            Log.i("customIsecon", "" + subFormattedScreeningCentersList);
            final Map<String, String> screeningCentersSubDataMap = new Gson().fromJson(subFormattedScreeningCentersList, Map.class);

            String centerName = screeningCentersSubDataMap.get("name").replace("_REAL_POINT_", ":");
            String centerDescription = screeningCentersSubDataMap.get("description").replace("_REAL_POINT_", ":");
            String centerEmail = screeningCentersSubDataMap.get("email").replace("_REAL_POINT_", ":");
            String centerPhoneNumber = screeningCentersSubDataMap.get("phone_number").replace("_REAL_POINT_", ":");
            String centerLatitude = screeningCentersSubDataMap.get("latitude").replace("_REAL_POINT_", ":");
            String centerLongitude = screeningCentersSubDataMap.get("longitude").replace("_REAL_POINT_", ":");
            String centerPhoto = screeningCentersSubDataMap.get("photo").replace("_REAL_POINT_", ":");

            Marker screeningCenterMarker = new Marker(map);
            screeningCenterMarker.setPosition(new GeoPoint(Double.parseDouble(centerLatitude), Double.parseDouble(centerLongitude)));
            screeningCenterMarker.setIcon(getResources().getDrawable(R.drawable.marker_default));
            screeningCenterMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            String popupContent = getResources().getString(R.string.map_marker_popup_content_format);
            popupContent = popupContent.replace("_NAME_", centerName);
            popupContent = popupContent.replace("_DESC_", centerDescription);
            popupContent = popupContent.replace("_TEL_", centerPhoneNumber);
            popupContent = popupContent.replace("_EMAIL_", centerEmail);
            screeningCenterMarker.setTitle(popupContent);
            map.getOverlays().add(screeningCenterMarker);

        }*/

        return true;
    }
}
