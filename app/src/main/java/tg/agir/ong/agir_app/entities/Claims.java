package tg.agir.ong.agir_app.entities;

public class Claims {
    private int id_claim;
    private int declaim;
    private String date_claim_occur;
    private String audio_content;
    private String video_content;
    private String text_content;
    private Double place_longitude;
    private Double place_latitude;
    private int place_district;
    private String saved_on;
    private int claim_status;
    private int delete_state;

    public Claims() {

    }

    public int getId_claim() {
        return id_claim;
    }

    public void setId_claim(int id_claim) {
        this.id_claim = id_claim;
    }

    public int getDeclaim() {
        return declaim;
    }

    public void setDeclaim(int declaim) {
        this.declaim = declaim;
    }

    public String getDate_claim_occur() {
        return date_claim_occur;
    }

    public void setDate_claim_occur(String date_claim_occur) {
        this.date_claim_occur = date_claim_occur;
    }

    public String getAudio_content() {
        return audio_content;
    }

    public void setAudio_content(String audio_content) {
        this.audio_content = audio_content;
    }

    public String getVideo_content() {
        return video_content;
    }

    public void setVideo_content(String video_content) {
        this.video_content = video_content;
    }

    public String getText_content() {
        return text_content;
    }

    public void setText_content(String text_content) {
        this.text_content = text_content;
    }

    public Double getPlace_longitude() {
        return place_longitude;
    }

    public void setPlace_longitude(Double place_longitude) {
        this.place_longitude = place_longitude;
    }

    public Double getPlace_latitude() {
        return place_latitude;
    }

    public void setPlace_latitude(Double place_latitude) {
        this.place_latitude = place_latitude;
    }

    public int getPlace_district() {
        return place_district;
    }

    public void setPlace_district(int place_district) {
        this.place_district = place_district;
    }

    public String getSaved_on() {
        return saved_on;
    }

    public void setSaved_on(String saved_on) {
        this.saved_on = saved_on;
    }

    public int getClaim_status() {
        return claim_status;
    }

    public void setClaim_status(int claim_status) {
        this.claim_status = claim_status;
    }

    public int getDelete_state() {
        return delete_state;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }
}
