package tg.agir.ong.agir_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import tg.agir.ong.agir_app.database_connexion.dao.ClaimsDAO;
import tg.agir.ong.agir_app.database_connexion.dao.Connection;
import tg.agir.ong.agir_app.database_connexion.dao.DAO;
import tg.agir.ong.agir_app.entities.Claims;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class AudioInstantDeclarationActivity extends AppCompatActivity {

    private Connection con;
    private Claims claim;
    private List<Claims> claimsList;

    private ListView listView;
    private ImageButton send_audio_declaration;
    private ImageButton restart;
    private ImageButton recordingstart;
    private AppCompatButton count_bt;

    final int REQUEST_PERMISSION_CODE = 1000;
    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;
    String path = "";
    String audio_name = "";
    File file1;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_instant_declaration);
        init();
        actions_onclick();
    }

    public void init(){
        con = new Connection(this);
        claim = new Claims();
        send_audio_declaration = findViewById(R.id.send_audio_instant_declaration);
        restart = findViewById(R.id.restart_audio_instant_declaration);
        recordingstart = findViewById(R.id.recording_instant_audio_start);
        count_bt = findViewById(R.id.count_instant_bt);
        handler = new Handler();
    }

    public void actions_onclick(){
        recordingstart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if(event.getAction() == MotionEvent.ACTION_DOWN){

                        if(checkAudioPermissionFromDevice()){
                            Toast.makeText(AudioInstantDeclarationActivity.this, "Recording ", Toast.LENGTH_SHORT).show();
                            setupMediaRecorder();
                            try {
                                mediaRecorder.prepare();
                                mediaRecorder.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            requestPermission();
                        }

                    }else if(event.getAction() == MotionEvent.ACTION_UP) {
                        mediaRecorder.stop();
                        listView = findViewById(R.id.audio_instant_listview);
                        claimsList = new ArrayList<>();

                        claimsList.add(claim);
                        AudioInstantDeclarationActivity.ListAdapter adapter = new AudioInstantDeclarationActivity.ListAdapter();
                        listView.setAdapter(adapter);
                        Toast.makeText(AudioInstantDeclarationActivity.this, "Fin ", Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){

                }
                return false;
            }
        });

        send_audio_declaration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(path == ""){
                    Toast.makeText(AudioInstantDeclarationActivity.this, "Vide", Toast.LENGTH_SHORT).show();
                }else{
                    final DAO<Claims> claimdao = new ClaimsDAO(con.getDb());
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    String currentDateTimeString = dateFormat.format(cal.getTime());
                    claim.setAudio_content(path);
                    claim.setSaved_on(currentDateTimeString);
                    claim.setDelete_state(0);
                    ((ClaimsDAO) claimdao).add_claims(claim);
                    Toast.makeText(AudioInstantDeclarationActivity.this, "Enregistrement réussi", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        }, REQUEST_PERMISSION_CODE);
    }

    private void setupMediaRecorder(){
        audio_name = UUID.randomUUID().toString()+"_audio_record.3gp";
        path = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/ong_agir/audio/"+audio_name;
        file1 = new File(path);

        mediaRecorder = new MediaRecorder();
        Log.i("record", mediaRecorder.toString());
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(path);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){

            case REQUEST_PERMISSION_CODE:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private boolean checkAudioPermissionFromDevice() {
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int recording_audio_result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                read_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                recording_audio_result == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isExternalStorageWritable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            Log.i("writeveri","is OK");
            return true;
        }
        else {
            return false;
        }
    }


    public class ListAdapter extends BaseAdapter {
        private View view;
        private SeekBar duration = null;
        private MediaPlayer mediaPlayer;
        private ImageButton play;
        private Runnable runnable;
        private Handler handler;
        private Boolean isPlaying = false;

        @Override
        public int getCount() {
            return claimsList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.audio_custom_view, null);
            duration = (SeekBar) view.findViewById(R.id.audio_custom_seekbar);
            play = (ImageButton) view.findViewById(R.id.audio_custom_play);

            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isPlaying == false) {
                        stopCurrentPlayAudio();
                        initAudioPlayer();
                        play.setImageResource(R.drawable.ic_pause_black_24dp);
                        mediaPlayer.start();
                        isPlaying = true;
                        Toast.makeText(AudioInstantDeclarationActivity.this, "Test playing audio", Toast.LENGTH_SHORT).show();
                    }else{
                        play.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        Log.i("Audiochange",play.toString());
                        mediaPlayer.pause();
                        isPlaying = false;

                    }

                }
            });

            return view;
        }

        public void initAudioPlayer() {
            try {
                if(mediaPlayer == null)
                {
                    mediaPlayer = new MediaPlayer();

                    if(path == "")
                    {
                        Toast.makeText(AudioInstantDeclarationActivity.this, "Veuillez Faire un enregistrement", Toast.LENGTH_SHORT).show();
                    }else {
                        mediaPlayer.setDataSource(path);
                        mediaPlayer.prepare();


                    }
                }
            }catch(IOException ex)
            {
                Toast.makeText(AudioInstantDeclarationActivity.this, ""+ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        public void stopCurrentPlayAudio() {
            if(mediaPlayer!=null && mediaPlayer.isPlaying())
            {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }

    }

}

