package tg.agir.ong.agir_app.entities;

public class Chat {
    private int id_chat;
    private int id_sender;

    //type_sender: 1=user ,2 = expert
    private int type_sender;
    private int id_receiver;
    //type_receiver: 1=user ,2 = expert
    private int type_receiver;
    private String content_text;
    private String content_audio;
    private String content_video;
    private String saved_on;
    private  int saved_by;
    private String modified_on;
    private int modified_by;
    //send =1 , receive=2, read=3, delete=3;
    private int delete_state;

    public void setId_chat(int id_chat) {
        this.id_chat = id_chat;
    }

    public void setId_sender(int id_sender) {
        this.id_sender = id_sender;
    }

    public void setType_sender(int type_sender) {
        this.type_sender = type_sender;
    }

    public void setId_receiver(int id_receiver) {
        this.id_receiver = id_receiver;
    }

    public void setType_receiver(int type_receiver) {
        this.type_receiver = type_receiver;
    }

    public void setContent_text(String content_text) {
        this.content_text = content_text;
    }

    public void setContent_audio(String content_audio) {
        this.content_audio = content_audio;
    }

    public void setContent_video(String content_video) {
        this.content_video = content_video;
    }

    public void setSaved_on(String saved_on) {
        this.saved_on = saved_on;
    }

    public void setSaved_by(int saved_by) {
        this.saved_by = saved_by;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public void setDelete_state(int delete_state) {
        this.delete_state = delete_state;
    }

    public int getId_chat() {
        return id_chat;
    }

    public int getId_sender() {
        return id_sender;
    }

    public int getType_sender() {
        return type_sender;
    }

    public int getId_receiver() {
        return id_receiver;
    }

    public int getType_receiver() {
        return type_receiver;
    }

    public String getContent_text() {
        return content_text;
    }

    public String getContent_audio() {
        return content_audio;
    }

    public String getContent_video() {
        return content_video;
    }

    public String getSaved_on() {
        return saved_on;
    }

    public int getSaved_by() {
        return saved_by;
    }

    public String getModified_on() {
        return modified_on;
    }

    public int getModified_by() {
        return modified_by;
    }

    public int getDelete_state() {
        return delete_state;
    }
}
